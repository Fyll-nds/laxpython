import sympy as sym
import Common.Simple as simp
from Common.Utility import tenProd_a

u, u_2 = sym.symbols("u u_2")

# This one doesn't use MatrixSeries, as it likely doesn't have a nice finite series.
class rMatrix:
    def __init__(self, ctsData):
        self.type = " "
        self.mat = sym.zeros(4, 4)

        self.sub = True
        self.addSP = True

        self.invLim = False
        self.spFct = 1.0
        self.low = 0

        self.ctsData = ctsData
        self.reset()


    # Resets all of the lists of terms.
    def reset(self):
        self.terms = []
        self.WrWTerms = []


    # Sets the type of r-matrix, the type of the spectral parameter, and the sign of the spectral parameter.
    def setType(self, setType, setAddSP, sub = True):
        self.type = setType
        self.addSP = setAddSP
        self.sub = sub


    # Marks that you're looking in the limit as u -> infinity.
    def setInvLim(self):
        self.invLim = True

        if(self.type == "y"):
            self.low = 1
        elif(self.type == "sg"):
            self.low = 0
        elif(self.type == "*"):
            self.findCustomLow()


    # If you are working with a custom r-matrix, find the lowest order term.
    def findCustomLow(self):
        holdLow = sym.Order(self.mat[0, 0], (u_2, (sym.oo if self.invLim else 0)))
        for i in range(0, 4, 1):
            for j in range(0, 4, 1):
                if(i > 0 or j > 0):
                    holdLim = sym.Order(self.mat[i, j], (u_2, (sym.oo if self.invLim else 0)))
                    if(holdLow + holdLim == holdLim):
                        holdLow = holdLim

        # The .args.args is so that it gets the lowest order term (as the Order object has args [lowest order term, ...]), and then the order of this term (as the Pow object has args [base, power]).
        if(holdLow != 0 and holdLow.args[0] != 1):
            if(isinstance(holdLow.args[0], sym.power.Pow)):
                self.low = holdLow.args[0].args[1] * (-1 if self.invLim else 1)
            else:
                self.low = (-1 if self.invLim else 1)
        else:
            self.low = 0


    # Edits the factor with regards to which the spectral parameter should be scaled.
    def scaleSP(self, fct):
        self.spFct *= fct


    # Sets the matrix to be the given matrix (only used when a custom r-matrix is being used).
    def setMat(self, mat):
        if(self.addSP):
            self.mat = mat.subs(u, u_2 - (u if self.sub else -u))
        else:
            self.mat = mat.subs(u, u_2 * ((1 / u) if self.sub else u))

        self.terms = []
        self.findCustomLow()


    # Sets the ctsData.
    def setWZData(self, datas):
        self.ctsData = data


    # Returns (-u) if you have parameter (u + v), for the pre-defined expansions below. Awkward, but the numbers just work out like that.
    def uSigned(self):
        return (1 if self.sub else -1) * u


    # Generates a new term for the expansion of the r-matrix if you've asked for a custom r-matrix.
    def addTerm(self):
        if(self.type == "*"):
            n = len(self.terms) + self.low

            holdMat = sym.zeros(4, 4)
            for i in range(0, 4, 1):
                for j in range(0, 4, 1):
                    holdEntry = self.mat[i, j].series(u_2, (sym.oo if self.invLim else 0), n + 1).removeO()
                    # If the series has multiple terms, we are only interested in the last one.
                    if(isinstance(holdEntry, sym.add.Add)):
                        holdEntry = holdEntry.args[-1]

                    # As we only have one term, to make sure it's the right term, 0 all excess powers of u_2.
                    holdMat[i, j] = (holdEntry * ((u_2 ** n) if self.invLim else (1 / (u_2 ** n)))).subs({ u_2: 0, (1 / u_2): 0 })

            self.terms.append(holdMat)


    # Gets the appropriate term from the expansion of the r-matrix, of order n.
    def getTerm(self, n):
        if(n < self.low):
            return 0

        # Store the output so that you can scale it later.
        outMat = sym.zeros(4, 4)

        # Checking the nice cases first: Yangian.
        if(self.type == "y"):
            if(self.invLim):
                outMat = (self.uSigned() ** (n - 1)) * sym.Matrix([[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]])
            else:
                outMat = -1 * sym.Matrix([[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]]) / (self.uSigned() ** (n + 1))
        # Checking the nice cases first: sinh-Gordon.
        elif(self.type == "sg"):
            if(n == 0):
                outMat = (1 if self.invLim else -1) * sym.Matrix([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 1]])
            else:
                outMat = (2 if self.invLim else -2) * sym.Matrix([[1 - (n % 2), 0, 0, 0], [0, 0, (n % 2), 0], [0, (n % 2), 0, 0], [0, 0, 0, 1 - (n % 2)]]) / (self.uSigned() ** n)
        # Else, calculate the term by looking at the series expansions.
        else:
            while(n - self.low >= len(self.terms)):
                self.addTerm()

            outMat = self.terms[n - self.low]

        # Scale the output by the appropriate factor, then return.
        outMat = (self.spFct ** n) * outMat.subs(u, u / self.spFct)
        for i in range(0, 4, 1):
            for j in range(0, 4, 1):
                outMat[i, j] = sym.simplify(outMat[i, j])

        return outMat


    # Adds a term to the expansion of (I + W)^{-1}_a r_{ab} (I + W)_a.
    def addWrWTerm(self):
        n = len(self.WrWTerms) + self.low
        self.WrWTerms.append(sym.zeros(4, 4))

        for i in range(0, n + 1, 1):
            for j in range(0, n + 1, 1):
                self.WrWTerms[-1] += tenProd_a(self.ctsData.getIpWInv(i)) * self.getTerm(j) * tenProd_a(self.ctsData.getIpW(n - i - j))

        # Simplify the result.
        for i in range(0, 4, 1):
            for j in range(0, 4, 1):
                self.WrWTerms[-1][i, j] = simp.applyConds(self.WrWTerms[-1][i, j])


    # Gets the term in the expansion of (I + W)^{-1}_a r_{ab} (I + W)_a of order n.
    def getWrWTerm(self, n):
        if(n < self.low):
            return sym.zeros(4, 4)

        while(n - self.low >= len(self.WrWTerms)):
            self.addWrWTerm()

        return self.WrWTerms[n - self.low]

