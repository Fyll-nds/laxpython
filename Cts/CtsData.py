import sympy as sym
import Common.Simple as simp
from Common.OutputStyling import boxedOup

u, x = sym.symbols("u x")

# A class for holding the W and Z matrices used in the construction of the continuous Lax pair and Hamiltonians.
class CtsData:
    def __init__(self, UMat):
        self.reset()
        self.U = UMat

        self.spFct = 1.0


    # Empties all of the lists that get built up.
    def reset(self):
        self.W = []
        self.IpWInvTerms = []
        self.Z = []


    # Scales the spactral parameter by the given factor.
    def scaleSP(self, fct):
        self.spFct = self.spFct * fct
         

    # Derives a new term in the W series.
    def addW(self):
        # Because it's used a lot, make a note of which order equation this is.
        n = len(self.W) + self.U.uLow()

        # Add a new W matrix with default values.
        w12 = sym.Symbol("w_12")
        w21 = sym.Symbol("w_21")
        self.W.append(sym.Matrix([[0, w12], [w21, 0]]))

        # Then construct the equation to solve.
        rhs = (self.spFct ** n) * (sym.zeros(2, 2) - self.U.getCoeff(False, n))

        for i in range(0, len(self.W), 1):
            rhs += (self.spFct ** (n - i)) * ((self.W[i] * self.U.getCoeff(True, n - i)) - (self.U.getCoeff(True, n - i) * self.W[i]))

        for i in range(0, len(self.W), 1):
            for j in range(0, len(self.W), 1):
                rhs += (self.spFct ** (n - i - j)) * (self.W[i] * self.U.getCoeff(False, n - i - j) * self.W[j])

        # Finally, add the differentiated term if the order is high enough.
        if(n >= 0):
            rhs += sym.Matrix([[0, self.W[n][0, 1].diff(x)], [self.W[n][1, 0].diff(x), 0]])

        # Now that you have the equation, solve it, then simplify the solutions.
        sols = list(sym.solve_poly_system([rhs[0, 1], rhs[1, 0]], w12, w21))

        # Choose the right roots if there are multiple (i.e. choose the roots that aren't each others' inverse).
        solIndex = 0
        while(solIndex < len(sols) and simp.applyConds((sols[solIndex][0] * sols[solIndex][1]) - 1) == 0):
            solIndex += 1

        if(solIndex < len(sols)):
            self.W[-1][0, 1] = simp.applyConds(sols[solIndex][0])
            self.W[-1][1, 0] = simp.applyConds(sols[solIndex][1])
        else:
            boxedOup("No suitable solution was found to the Ricatti-style W-matrix relation.")


    # Add a new Z matrix to the array.
    def addZ(self):
        n = len(self.Z) + self.U.uLow()
 
        self.Z.append((self.spFct ** n) * self.U.getCoeff(True, n))
        for i in range(0, len(self.U.adiag.degs), 1):
            self.Z[-1] += (self.spFct ** self.U.adiag.degs[i]) * (self.U.adiag.mats[i] * self.getW(n - self.U.adiag.degs[i]))

        # Scale the Z-matrix by the appropriate factor coming from the spectral parameter.
        self.Z[-1] = simp.applyConds(self.Z[-1])


    # Gets the W-matrix of the given index, and adds one if there isn't already one.
    def getW(self, n):
        if(n < 0):
            return sym.zeros(2, 2)

        while(n >= len(self.W)):
            self.addW()

        return self.W[n]


    # Gets the Z-matrix of the given index, and adds one if there isn't already one.
    def getZ(self, n):
        if(n < 0):
            return sym.zeros(2, 2)

        while(n >= len(self.Z)):
            self.addZ()

        return self.Z[n]


    # Returns true if the largest real coefficient of the lowest, non trivial order of Z is in the top-left. Stands for ZTopLeftGreaterThanBottomRight.
    def Ztlgtbr(self, n = 0):
        return (sym.re(sym.expand((self.getZ(n)[0, 0] - self.getZ(n)[1, 1]) / (self.spFct ** (n + self.U.uLow())))) > 0)


    # For getting (I + W).
    def getIpW(self, n):
        if(n == 0):
            return sym.eye(2) + self.getW(0)
        else:
            return self.getW(n)


    # Recursion function used in getIpWInv().
    def recFuncIpW(self, n):
        out = -1 * self.getW(n)
        for i in range(1, n, 1):
            out -= self.getW(i) * self.recFuncIpW(n - i)

        return out


    # Gets the appropriate term from (I + W)^(-1) of order n.
    def getIpWInv(self, n):
        if(n < 0):
            return 0
        elif(len(self.IpWInvTerms) == 0):
            self.IpWInvTerms.append((sym.eye(2) + self.getW(0)).inv())

        while(n >= len(self.IpWInvTerms)):
            self.IpWInvTerms.append(self.IpWInvTerms[0] * self.recFuncIpW(len(self.IpWInvTerms)) * self.IpWInvTerms[0])

        return self.IpWInvTerms[n]

