import sympy as sym

import Cts.CtsData as CtsData
import Cts.CtsHamiltonians as Hams
import Cts.VMatrices as VMatrices
import Common.LaxMatrix as LaxMatrix
import Cts.rMatrix as rMatrix
import Cts.KMatrix as KMatrix
import Common.Simple as simp
import Common.InputParsing as inp
import Common.OutputStyling as oup

# The spectral parameter(s if using r-matrices) and variables.
u, u_2, x, L = sym.symbols("u u_2 x L")

# Declared up here, but actually initialised down once things has been passed in.
UMat = LaxMatrix.LaxMatrix()
ctsData = CtsData.CtsData(UMat)

# r-matrix stuff in case you're asked about V-matrices.
rMat = rMatrix.rMatrix(ctsData)
invLim = False

# K-matrix stuff in case you're asked about open BCs. Plus the hatted versions of U and W/Z.
openBCs = False
UMatHat = LaxMatrix.LaxMatrix()
ctsDataHat = CtsData.CtsData(UMatHat)
# KMats[0] = (K+), KMats[1] = (K-)
KMats = [ KMatrix.KMatrix([ctsDataHat, ctsData], L), KMatrix.KMatrix([ctsData, ctsDataHat], -L) ]

# Another r-matrix for if you want an open chain.
rMatOpen = rMatrix.rMatrix(ctsDataHat)

# The Hamiltonians and V-matrices.
hams = Hams.Hamiltonians([ctsData, ctsDataHat], KMats)
VMats = VMatrices.VMatrices(UMat, KMats, rMat, rMatOpen)


#---------------------------------------------------------------------------------------------------------------------#
#                                          --v-- The function calling --v--                                           #
#---------------------------------------------------------------------------------------------------------------------#

# Converts a number into a bracketed index (and reverses it if you're in the opposite limit).
def nToIndex(n):
    if(invLim):
        n *= -1
    if(n < 0 or n > 9):
        return "{" + str(n) + "}"
    else:
        return str(n)


# Functions for passing in to the below function.
def getVbMat(i):
    return VMats.getVMat(i, 0)
def getVpMat(i):
    return VMats.getVMat(i, 1)
def getVmMat(i):
    return VMats.getVMat(i, -1)


# Prints the information to be printed.
def innerPrintFunc(func, low, high, printChar, shift, cliOut, texOut, texName):
    if(cliOut):
        oup.printSepLine()
    if(texOut):
        oup.addTexSepLine()
        oup.addTexText(" \\textbf{" + texName + ":} ")

    for i in range(low, high + 1, 1):
        holdText = printChar + "_" + nToIndex(i + shift)
        holdOut = func(i)

        # If you're printing to the terminal, do so.
        if(cliOut):
            print("\n" + holdText + ":")
            sym.pprint(holdOut)

        # If you're printing to TeX, do so.
        if(texOut):
            oup.addTexEq(holdText, holdOut)


# Prints out everything requested with spacings.
def printCtsResults(n, which, cliOut, texOut):
    # Now try fetching the number of Ham.'s requested, if asked.
    if(which[0]):
        hams.init(UMat.uLow(), openBCs)
        innerPrintFunc(hams.getHam, hams.low, n, 'H', 0, cliOut, texOut != "n", "Hamiltonians")

    # Try fetching the appropriate number of V-matrices, if you want.
    if(which[1]):
        VMats.init([ctsData, ctsDataHat])
        innerPrintFunc(getVbMat, UMat.uLow(), n, 'V', 0, cliOut, texOut != "n", "V-Matrices" + (" (bulk)" if openBCs else ""))

        # If it's an open chain, do the boundarys as well.
        if(openBCs):
            innerPrintFunc(getVpMat, UMat.uLow(), n, 'V^{+}', 0, cliOut, texOut != "n", "V-Matrices (+L boundary)")
            innerPrintFunc(getVmMat, UMat.uLow(), n, 'V^{-}', 0, cliOut, texOut != "n", "V-Matrices (-L boundary)")

    # If you've been asked to print out the W's and Z's, do so.
    if(which[2]):
        innerPrintFunc(ctsData.getW, 0, len(ctsData.W), 'W', 0, cliOut, texOut != "n", "W-Matrices")

    if(which[3]):
        innerPrintFunc(ctsData.getZ, 0, len(ctsData.Z), 'Z', UMat.uLow(), cliOut, texOut != "n", "Z-Matrices")

    # Preview the TeX if you've been asked.
    if(texOut != "n"):
        oup.previewTex(texOut[1:])


#---------------------------------------------------------------------------------------------------------------------#
#                                             --v-- The input reading --v--                                           #
#---------------------------------------------------------------------------------------------------------------------#

# Actually initialise all of the variables.
def initCts(inpMats, inpNonMats):
    # Allow this function to edit the globals.
    global UMat, ctsData, rMat, invLim, negLim, zLim, openBCs, UMatHat, ctsDataHat, KMats, rMatOpen, VMats

    # Empty all of the previously calculated results (in case you're running through the GUI).
    ctsData.reset()
    rMat.reset()
    ctsDataHat.reset()
    rMatOpen.reset()

    # Set up the U-matrix and r-matrix.
    UMat.setMat(inpMats[0].getWhole())
    rMat.setType(inpNonMats[2], inpNonMats[1])
    rMat.setMat(inpMats[1])

    # If you want open, set all of that up.
    openBCs = inpNonMats[0]
    if(openBCs):
        UMatHat.setMat(inpMats[0].getWhole())
        KMats[0].setMat(inpMats[2].getWhole())
        KMats[1].setMat(inpMats[3].getWhole())
        VMats.openBCs = True

        rMatOpen.setType(inpNonMats[2], inpNonMats[1], False)
        rMatOpen.setMat(inpMats[1])

        if(inpNonMats[1]):
            UMatHat.cffLim(-1)
        else:
            UMatHat.invLim()

    # If they want a factor in front of the limit, apply that here.
    if(inpNonMats[4] != 1.0):
        UMat.cffLim(inpNonMats[4])
        rMat.scaleSP(inpNonMats[4])
        ctsData.scaleSP(inpNonMats[4])

        if(openBCs):
            KMats[0].cffLim(inpNonMats[4])
            KMats[1].cffLim(inpNonMats[4])
            UMatHat.cffLim(inpNonMats[4])
            rMatOpen.scaleSP(inpNonMats[4])
            ctsDataHat.scaleSP(inpNonMats[4])

    # If they actually want the u -> inf limit, just invert all of the u's.
    if(inpNonMats[3].find("inf") >= 0):
        invLim = True

        UMat.invLim()
        rMat.setInvLim()

        if(openBCs):
            KMats[0].invLim()
            KMats[1].invLim()
            UMatHat.invLim()
            rMatOpen.setInvLim()


# Read in the inputs.
def runCts():
    # Listed objects: [UMat, rMat, KMats[0], KMats[1]], [openBCs, additive parameter, rMat type, invLim, limit factor]
    inpMats = [inp.MatrixWrapper(), sym.zeros(4, 4), inp.MatrixWrapper(), inp.MatrixWrapper()]
    inpNonMats = [False, True, " ", "0", 1.0]

    # Get the entries of the U-matrix.
    inp.parseMatrix(inpMats[0], "U")
    
    # Get any conditions on the fields.
    print("\nAnd are there any extra conditions on the fields ('y'/'n')?")
    if(inp.inpInList(["y", "n"]) == "y"):
        print("\nThen please enter them, one at a time (e.g. 'a(x) + b(x) = 1'), until you are done (and then just enter a blank).")
        holdInp = inp.getInp()

        while(holdInp != ""):
            simp.addCond(holdInp)
            holdInp = inp.getInp()

    # Are you considering an open or closed system?
    print("\nWould you like to consider open ('o') or closed ('c') boundary conditions?")
    inpNonMats[0] = (inp.inpInList(["o", "c"]) == "o")

    # If they said "open", get them to enter the K-matrices.
    if(inpNonMats[0]):
        inp.parseMatrix(inpMats[2], "(K+)")
        inp.parseMatrix(inpMats[3], "(K-)")

    print("\nIs the spectral parameter additive ('+') or multiplicative ('*')?")
    inpNonMats[1] = (inp.inpInList(["+", "*"]) == "+")

    # Do you want H, W, Z, and/or, V-matrices?
    print("\nDo you want to be given Hamiltonians ('H'), W-matrices ('W'), Z-matrices ('Z'), and/or V-matrices ('V'" + (", which includes both Bulk and Boundary" if inpNonMats[0] else "") + ")?")
    optsHWZV = inp.getInp()

    # If so, fetch the r-matrix.
    if('V' in optsHWZV):
        print("\nIn that case, please choose an r-matrix. The options are Yangian ('Y'), sinh-Gordon ('sG'), or a custom one ('*'):")
        inpNonMats[2] = inp.inpInList(["y", "sg", "*"])

        # If they want a custom r-matrix, have them input it.
        if(inpNonMats[2] == "*"):
            inpMats[1] = inp.parse4Matrix("r")

    # Which limit would you like to consider?
    print("\nWhat limit would you like to consider? Choose for u to go to either 0 ('0') or infinity ('inf'), and if you want a factor in front (such as -), preface it with 'a'.")
    inpNonMats[3] = inp.inpInList(["0", "a0", "inf", "ainf"])
    if(inpNonMats[3][0] == "a"):
        print("\nPlease enter the factor.")
        inpNonMats[4] = inp.parseInp()

    # Find out how many terms to find.
    print("\nWhat is the highest order of " + ("1/u" if inpNonMats[3] else "u") + " you wish to calculate up to?")
    nTerms = inp.readInt()

    # Do you want TeX?
    print("\nWould you like to try to view the results with LaTeX formatting ('y'/'n'/'save')?")
    doTex = inp.inpInList(["y", "n", "save"])
    if(doTex == "save"):
        print("\nPlease enter the file name to use.")
        doTex = "s" + inp.getInp()

    # Run the actual program.
    initCts(inpMats, inpNonMats)
    printCtsResults(nTerms, ['H' in optsHWZV, 'V' in optsHWZV, 'W' in optsHWZV, 'Z' in optsHWZV], True, doTex)

