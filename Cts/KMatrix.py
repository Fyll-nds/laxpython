import sympy as sym
import Common.MatrixSeries as MatrixSeries
import Common.Simple as simp

u, x = sym.symbols("u x")
 
class KMatrix:
    def __init__(self, ctsDatas, edge):
        self.edge = edge
         
        self.mat = MatrixSeries.MatrixSeries()
        self.ctsDatas = ctsDatas
        self.terms = []
         
         
    # A few drop through functions.
    def setMat(self, mat):
        self.mat.setMat(mat)
    def getWhole(self):
        return self.mat.getWhole()
    def getCoeff(self, n):
        return self.mat.getCoeff(n)
    def invLim(self):
        self.mat.invLim()
    def cffLim(self, cff):
        self.mat.cffLim(cff)
    def uLow(self):
        return self.mat.uLow
         
         
    # Adds a term to the expansion of (I + W)^{-1} K (I + W).
    def addTerm(self):
        n = len(self.terms) - self.uLow()
        self.terms.append(sym.zeros(2, 2))

        for i in range(0, len(self.mat.degs), 1):
            for j in range(0, n - self.mat.degs[i] + 1, 1):
                self.terms[-1] += self.ctsDatas[0].getIpWInv(j) * self.mat.mats[i] * self.ctsDatas[1].getIpW(n - self.mat.degs[i] - j)

        # Now evaluate it at the appropriate limit.
        self.terms[-1] = simp.applyConds(self.terms[-1]).subs(x, self.edge)
         
         
    # Gets the term of order n in the expansion of (I + W)^{-1} K (I + W).
    def getTerm(self, n):
        if(n < self.uLow()):
            return sym.zeros(2, 2)
             
        while(n - self.uLow() >= len(self.terms)):
            self.addTerm()
             
        return self.terms[n - self.uLow()]
