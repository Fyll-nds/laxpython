import sympy as sym
from Common.Utility import tenProd_a, tenProd_b, ptr_a
import Common.Simple as simp

x, L = sym.symbols("x L")

# A class for storing the V-matrices in.
class VMatrices:
    def __init__(self, UMat, KMats, rMat, rMatOpen):
        self.UMat = UMat
        self.Ztlgtbr = [False, False]

        self.openBCs = False
        self.KMats = KMats
        self.rMats = [rMat, rMatOpen]


    # Initialises the data.
    def init(self, ctsDatas):
        # Ordered (-L, x, L).
        self.VMats = [[], [], []]

        # The second one is 'not'd, as T-hat = T^{-1}(-u), so the e^{Z} -> e^{-Z}.
        self.Ztlgtbr = [bool(ctsDatas[0].Ztlgtbr()), (not bool(ctsDatas[1].Ztlgtbr()) if self.openBCs else False)]


    # Returns the numerator (i.e. not t^{-1}) in the expansion of the V-matrix located around the pm K-matrix.
    # Explicitly, we need to evaluate: ptr{ZBit * (I + W)^{-1} * ((r^{pm} * K^{pm}) + (K^{pm} * r^{mp})) * (I + W)}
    def getVMatNumerator(self, n, ZBit, pm):
        out = sym.zeros(2, 2)

        # As K has a finite number of degrees, iterate over that first.
        for i in range(0, len(self.KMats[pm].mat.degs), 1):
            ki = self.KMats[pm].mat.degs[i]

            # Then do everything else in whatever order.
            for j in range(0, n + 1 - ki, 1):
                for l in range(0, n + 1 - ki - j, 1):
                    out += ptr_a(tenProd_a(ZBit * self.KMats[pm].ctsDatas[0].getIpWInv(j)) * ((self.rMats[pm].getTerm(l) * tenProd_a(self.KMats[pm].getCoeff(ki))) + (tenProd_a(self.KMats[pm].getCoeff(ki)) * self.rMats[1 - pm].getTerm(l))) * tenProd_a(self.KMats[pm].ctsDatas[1].getIpW(n - ki - j - l)))

        return out


    # Uses recursion to find the appropriate term in the expansion of the denominator.
    def recVMatDenominator(self, ZBit, div, pm, n):
        if(n == 0):
            return 1

        out = 0
        for i in range(1, n + 1, 1):
            out -= sym.trace(ZBit * self.KMats[pm].getTerm(i)) * self.recVMatDenominator(ZBit, div, pm, n - i) / div

        return out


    # Derive the V-matrices. If you are considering open BCs, you can look at one of the boundaries (+-1).
    def addVMat(self, bound = 0):
        n = len(self.VMats[bound + 1]) + self.UMat.uLow() + self.rMats[0].low

        # This is the nicest case, so lets look at this first.
        if(self.UMat.uLow() < 0):
            # If the tl entry of Z and ZHat is the positive one.
            useTL = [int(self.Ztlgtbr[0]), int(self.Ztlgtbr[1])]

            # For the nice easy closed case: Multiply by the appropriate diagonal matrix and take the trace.
            if(not self.openBCs):
                self.VMats[1].append(simp.applyConds(ptr_a(tenProd_a(sym.Matrix([[useTL[0], 0], [0, 1 - useTL[0]]])) * self.rMats[0].getWrWTerm(n))))
            # For bulk, it's just the sum of the two cases.
            elif(bound == 0):
                holdMat = ptr_a(tenProd_a(sym.Matrix([[useTL[0], 0], [0, 1 - useTL[0]]])) * self.rMats[0].getWrWTerm(n))
                holdMat += ptr_a(tenProd_a(sym.Matrix([[useTL[1], 0], [0, 1 - useTL[1]]])) * self.rMats[1].getWrWTerm(n))

                self.VMats[1].append(simp.applyConds(holdMat))
            # The boundary cases are, unfortunately, the most complicated.
            else:
                # Mapping the +L boundary to 0 and the -L boundary to 1 for the KMats index.
                pm = (1 - bound) // 2

                ZBit = sym.Matrix([[int(useTL[0] == 1 and useTL[1] == 1), int(useTL[0] == 1 - pm and useTL[1] == pm)], [int(useTL[0] == pm and useTL[1] == 1 - pm), int(useTL[0] == 0 and useTL[1] == 0)]])
                div = sym.trace(ZBit * self.KMats[pm].getCoeff(self.KMats[pm].uLow()))

                out = sym.zeros(2, 2)
                # Get every combination of orders of t^{-1} and tr{...} that sums to n.
                for i in range(self.KMats[pm].uLow(), n + 1, 1):
                    out += self.getVMatNumerator(n - i, ZBit, pm) * self.recVMatDenominator(ZBit, div, pm, i) / div

                self.VMats[bound + 1].append(simp.applyConds(out).subs(x, bound * L))
        else:
            print("I'm sorry, but I don't know how to cleanly write the series of V-matrices for this choice of U- and r-matrices.")
            self.VMats[1].append(sym.zeros(2, 2))


    # Fetch the V-matrix of the given order, in the given limit.
    def getVMat(self, n, bound = 0):
        if(n < self.UMat.uLow() + self.rMats[0].low or abs(bound) > 1):
            return sym.zeros(2, 2)

        while(n - self.UMat.uLow() - self.rMats[0].low >= len(self.VMats[bound + 1])):
            self.addVMat(bound)

        return self.VMats[bound + 1][n - self.UMat.uLow() - self.rMats[0].low]

