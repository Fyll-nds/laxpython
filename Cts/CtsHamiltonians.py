import sympy as sym
import Cts.CtsData as CtsData
import Common.Simple as simp

# A class for managing all of the Hamiltonians.
class Hamiltonians:
    def __init__(self, ctsDatas, KMats):
        self.ctsDatas = ctsDatas
        self.KMats = KMats

        self.low = 0
        self.openBCs = False

        self.KLow = 0
        self.ZBits = [sym.zeros(2, 2), sym.zeros(2, 2)]


    # For initialising manually once everything has been passed in.
    def init(self, low, openBCs):
        self.hams = []
        self.bndTrs = []

        self.low = low
        self.openBCs = openBCs

        # If you're going to be looking at open BCs, work out a few things here for later.
        if(self.openBCs):
            self.ZBits[0] = (sym.Matrix([[1, 0], [0, 0]]) if self.ctsDatas[0].Ztlgtbr() else sym.Matrix([[0, 0], [0, 1]]))
            self.ZBits[1] = (sym.Matrix([[0, 0], [0, 1]]) if self.ctsDatas[1].Ztlgtbr() else sym.Matrix([[1, 0], [0, 0]]))

            self.KLow = 0
            self.KLowDeg = -1
            while(self.KLow == 0):
                self.KLowDeg += 1
                holdMat = sym.zeros(2, 2)
                for i in range(0, self.KLowDeg + 1, 1):
                    holdMat += self.ZBits[0] * self.KMats[1].getTerm(self.KMats[1].uLow() + i) * self.ZBits[1] * self.KMats[0].getTerm(self.KMats[0].uLow() + self.KLowDeg - i)

                self.KLow = holdMat[0, 0] + holdMat[1, 1]

            # Make a note of the lowest order of the boundary terms.
            self.KLowDeg = self.KLowDeg + self.KMats[0].uLow() + self.KMats[1].uLow()


    # Recursion function used to find the term of order n from the logarithm of the boundaries (only used for open BCs).
    def recFuncLogBnds(self, n, depth = 1):
        # Less than 1, as the 0 case is factored out from the log.
        if(n < 1):
            return 0

        out = self.getBndTerm(n) / depth
        for i in range(1, n, 1):
            out -= self.getBndTerm(i) * self.recFuncLogBnds(n - i, depth + 1)

        return out


    # Add a new Hamiltonian to the list.
    def addHam(self):
        if(self.low < 0):
            n = len(self.hams)

            # Nice easy periodic system.
            if(not self.openBCs):
                if(self.ctsDatas[0].Ztlgtbr()):
                    self.hams.append(simp.fakeIntegral(self.ctsDatas[0].getZ(n)[0, 0]))
                else:
                    self.hams.append(simp.fakeIntegral(self.ctsDatas[0].getZ(n)[1, 1]))
            # Complicated open BCs.
            else:
                # Make a note of the non-boundary part of the Hamiltonian.
                self.hams.append(self.ctsDatas[0].getZ(n)[0, 0] if self.ctsDatas[0].Ztlgtbr() else self.ctsDatas[0].getZ(n)[1, 1])
                self.hams[-1] -= (self.ctsDatas[1].getZ(n)[1, 1] if self.ctsDatas[1].Ztlgtbr() else self.ctsDatas[1].getZ(n)[0, 0])
                self.hams[-1] = simp.fakeIntegral(self.hams[-1], False)

                # Finally, add the appropriate term from the log expansion onto the Hamiltonian.
                if(n + self.low == 0):
                    self.hams[-1] += sym.log(self.KLow)
                else:
                    self.hams[-1] += self.recFuncLogBnds(n + self.low)

                self.hams[-1] = simp.applyConds(self.hams[-1])
        # Things have gone to pot.
        else:
            print("Things have gone to pot.")
            self.hams.append(0)
            if(self.openBCs):
                self.bndHams.append(0)


    # Get the nth Hamiltonian.
    def getHam(self, n):
        if(n < self.low):
            return 0

        while(n - self.low >= len(self.hams)):
            self.addHam()

        return self.hams[n - self.low]


    # Adds a term to the expansion of tr{...} for the boundary terms.
    def addBndTerm(self):
        n = len(self.bndTrs) + self.KLowDeg + 1
        self.bndTrs.append(0)

        for i in range(0, n + 1, 1):
            index = self.KMats[1].uLow() + i
            holdMat = self.ZBits[0] * self.KMats[1].getTerm(index) * self.ZBits[1] * self.KMats[0].getTerm(n - index)
            self.bndTrs[-1] += (holdMat[0, 0] + holdMat[1, 1]) / self.KLow


    # Gets the boundary term of order n, after rescaling by KLowDeg (i.e. rescaling so that the lowest order is order 1).
    def getBndTerm(self, n):
        if(n < 1):
            return 0

        while(n - 1 >= len(self.bndTrs)):
            self.addBndTerm()

        return self.bndTrs[n - 1]
