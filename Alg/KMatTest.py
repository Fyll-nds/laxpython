from sympy import symbols, zeros, simplify
from Common.Utility import tenProd_a, tenProd_b

# The function that checks if the given K- and r-matrices satisfy the appropriate algebraic relation (due to Sklyanin '87).
def testKMat(KMat, rMat, spAdd):
    # Everything should be functions only of u already.
    u, u_2 = symbols("u u_2")

    # Put everything into the correct tensor spaces and make everything be functions of the correct variables.
    KMat_a = tenProd_a(KMat)
    KMat_b = tenProd_b(KMat.subs(u, u_2))
    rMat_p = rMat.subs(u, ((u + u_2) if spAdd else (u * u_2)))
    rMat_m = rMat.subs(u, ((u - u_2) if spAdd else (u / u_2)))

    # The relation is: (r-)(Ka)(Kb) - (Ka)(Kb)(r-) + (Ka)(r+)(Kb) - (Kb)(r+)(Ka) = 0
    return (simplify((rMat_m * KMat_a * KMat_b) - (KMat_a * KMat_b * rMat_m) + (KMat_a * rMat_p * KMat_b) - (KMat_b * rMat_p * KMat_a)) == zeros(4, 4))
