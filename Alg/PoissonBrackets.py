import sympy as sym
from Common.Utility import tenProd_a, tenProd_b
import Common.MatrixSeries as MatSeries
import Common.OutputStyling as oup

# So they're available.
u, u_2, x, n, y, m = sym.symbols("u u_2 x n y m")

# Checks if the given expression is constant with respect to the given variable type.
def isConst(expr, disc):
    return ((disc and sym.diff(expr, n) == 0) or (not disc and sym.diff(expr, x) == 0))


# Prints the Poisson Brackets, given the appropriate RHS.
def printPBs(disc, lax, rhs, cliOut, texOut):
    laxSeries = MatSeries.MatrixSeries()
    laxSeries.setMat(lax)

    PBList = []

    # Print out the PBs, line by line, and term by term.
    for i in range(0, 4, 1):
        for j in range(0, 4, 1):
            # As the RHS might have complicated factors, split it into [numerator, denominator].
            rhsFrac = sym.fraction(sym.cancel(rhs[i, j]))
            rhsSeries = [sym.Poly(rhsFrac[0], u, u_2), sym.Poly(rhsFrac[1], u, u_2)]
            rhsDgs = [rhsSeries[0].monoms(), rhsSeries[1].monoms()]
            rhsCfs = [rhsSeries[0].coeffs(), rhsSeries[1].coeffs()]

            # Go through each term in the product of the rhs denominator, the left term, and the right term.
            for k in range(0, len(rhsDgs[1]), 1):
                for l in range(0, len(laxSeries.degs), 1):
                    for p in range(0, len(laxSeries.degs), 1):
                        # Add a new element in the form: [lhs_a, lhs_b, rhs]
                        PBList.append([laxSeries.mats[l][i // 2, j // 2], laxSeries.mats[p][i % 2, j % 2], 0])

                        # Find the corresponding term in the expansion of the rhs numerator.
                        for q in range(0, len(rhsDgs[0]), 1):
                            if(rhsDgs[0][q][0] == rhsDgs[1][k][0] + laxSeries.degs[l] and rhsDgs[0][q][1] == rhsDgs[1][k][1] + laxSeries.degs[p]):
                                PBList[-1][2] = rhsCfs[0][q] / rhsCfs[1][k]
                                break

            # Keep an eye out for any terms on the rhs that don't have an equivalent on the lhs.
            for k in range(0, len(rhsDgs[0]), 1):
                step = 0

                for l in range(0, len(laxSeries.degs), step + 1):
                    for p in range(0, len(laxSeries.degs), step + 1):
                        for q in range(0, len(rhsDgs[1]), step + 1):
                            if(rhsDgs[0][k][0] == rhsDgs[1][q][0] + laxSeries.degs[l] and rhsDgs[0][k][1] == rhsDgs[1][q][1] + laxSeries.degs[p]):
                                step = len(laxSeries.degs) + len(rhsDgs[1])

                if(step == 0):
                    PBList.append([0, 0, rhsCfs[0][k]])

    # Remove any trivial results and parts. Iterates backwards because deleting inside Python loops is wierd.
    for i in range(len(PBList) - 1, -1, -1):
        # Remove any constant terms from two bits of the PB.
        for j in range(0, 2, 1):
            holdTerm = 0
            # Go over each term in the sum if there is a sum.
            if(PBList[i][j] != 0 and PBList[i][j].func == sym.Add):
                for arg in PBList[i][j].args:
                    if(not isConst(arg, disc)):
                        holdTerm += arg
            elif(not isConst(PBList[i][j], disc)):
                holdTerm += PBList[i][j]

            PBList[i][j] = holdTerm

        # Now if one of the PB bits was constant, and the rhs is constant, just delete this entry.
        if((PBList[i][0] == 0 or PBList[i][1] == 0) and isConst(PBList[i][2], disc)):
            del PBList[i]

    # If you didn't find any, say so.
    if(len(PBList) == 0):
        if(cliOut):
            print("\nNo non-trivial Poisson Brackets (or other relations) were found....")
        elif(texOut):
            oup.addTexText("No non-trivial Poisson Brackets (or other relations) were found....")
            oup.previewTex()

        return
    # If there were PBs, print them (after doing a bit of cleaning).
    else:
        # If the contents of a PB are a product with a scalar, factor out the scalar.
        for i in range(0, len(PBList), 1):
            # Check the first term in the PB, then the second.
            for j in range(0, 2, 1):
                holdFct = 1

                # Find all of the constant factors from the current side of the PB.
                if(PBList[i][j] != 0 and PBList[i][j].func == sym.Mul):
                    for arg in PBList[i][j].args:
                        if(isConst(arg, disc)):
                            holdFct *= arg

                # Scale those factors out of the PB and into the rhs.
                PBList[i][j] /= holdFct
                PBList[i][2] /= holdFct

        # Remove redundancies (counts backwards as it's cutting from the list).
        for i in range(len(PBList) - 1, -1, -1):
            for j in range(i - 1, -1, -1):
                if((PBList[i][0] == PBList[j][0] and PBList[i][1] == PBList[j][1] and PBList[i][2] - PBList[j][2] == 0) or (PBList[i][0] == PBList[j][1] and PBList[i][1] == PBList[j][0] and PBList[i][2] + PBList[j][2] == 0)):
                    del PBList[i]
                    break

        # Which delta function should you be using?
        holdDelta = (sym.KroneckerDelta(n, m) if disc else sym.DiracDelta(x - y))

        # Change all of the second terms to be functions of y/m instead of x/n.
        for i in range(0, len(PBList), 1):
            if(PBList[i][1] != 0):
                PBList[i][1] = PBList[i][1].subs((n if disc else x), (m if disc else y))

        # Print in the console.
        if(cliOut):
            for i in range(0, len(PBList), 1):
                if(PBList[i][0] != 0 and PBList[i][1] != 0):
                    sym.pprint(sym.Eq(sym.Symbol("{" + str(PBList[i][0]) + ", " + str(PBList[i][1]) + "}"), PBList[i][2] * holdDelta, evaluate = False))
                else:
                    sym.pprint(sym.Eq(0, PBList[i][2], evaluate = False))

        # Do the TeX.
        if(texOut != "n"):
            for i in range(0, len(PBList), 1):
                if(PBList[i][0] != 0 and PBList[i][1] != 0):
                    oup.addTexEq(sym.Symbol("\\lbrace " + str(PBList[i][0]) + ", " + str(PBList[i][1]) + "\\rbrace "), PBList[i][2] * holdDelta)
                else:
                    oup.addTexEq(0, PBList[i][2])

            oup.previewTex(texOut[1:])


# Calculates the RHS for a linear algebra.
def linAlgRHS(lax, rMat, spAdd):
    # Define your spectral parameters.
    u, u_2 = sym.symbols("u u_2")

    # Put everything in the right tensor space, and make sure it has the right parameters.
    lax_a = tenProd_a(lax)
    lax_b = tenProd_b(lax.subs(u, u_2))
    rMat_m = rMat.subs(u, ((u - u_2) if spAdd else (u / u_2)))

    # Return [r, (Ua) + (Ub)].
    return (rMat_m * (lax_a + lax_b)) - ((lax_a + lax_b) * rMat_m)


# Calculates the RHS for a quadratic algebra.
def quadAlgRHS(lax, rMat, spAdd):
    # Define your spectral parameters.
    u, u_2 = sym.symbols("u u_2")

    # Put everything in the right tensor space, and make sure it has the right parameters.
    lax_a = tenProd_a(lax)
    lax_b = tenProd_b(lax.subs(u, u_2))
    rMat_m = rMat.subs(u, ((u - u_2) if spAdd else (u / u_2)))

    # Return [r, (La) * (Lb)].
    return (rMat_m * lax_a * lax_b) - (lax_a * lax_b * rMat_m)

