import sympy as sym
import Common.InputParsing as inp
import Common.OutputStyling as oup

x, n, t, m, u = sym.symbols("x n t m u")

# Returns a list containing the EoMs (without duplicates) from the ZCC.
def getEoMs(xMat, tMat, disc, cutDupes = True):
    eom = 0

    # Calculate the EoMs from:
    # 0 = U_t - V_x + [U, V]
    if(disc == 'c'):
        eom = xMat.diff(t) - tMat.diff(x) + (xMat * tMat) - (tMat * xMat)
    # 0 = L_{n, t} - A_{n + 1} * L_n - L_n * A_n
    elif(disc == 'd'):
        eom = xMat.diff(t) - (tMat.subs(n, n + 1) * xMat) + (xMat * tMat)
    # 0 = L_{n, m + 1} * A_{n, m} - A_{n + 1, m} * L_{n, m}
    elif(disc == 'f'):
        eom = (xMat.subs(m, m + 1) * tMat) - (tMat.subs(n, n + 1) * xMat)

    # Put all of the non-zero EoMs into a list.
    eomList = []
    for i in range(0, 2, 1):
        for j in range(0, 2, 1):
            eoms = sym.Poly(sym.simplify(eom[i, j]), u, 1/u).coeffs()
            for eq in eoms:
                if(eq != 0):
                    eomList.append(eq)

    # Remove any repeated ones (if requested).
    if(cutDupes):
        for i in range(len(eomList) - 1, -1, -1):
            for j in range(i - 1, -1, -1):
                if(eomList[i].subs(eomList[j], 0) == 0):
                    del eomList[i]
                    break

    return eomList


# Print the EoMs returned by the above function through the requetsed means.
def printEoMs(xMat, tMat, disc, cliOut, texOut, cutDupes = True):
    eomList = getEoMs(xMat, tMat, disc, cutDupes)

    if(cliOut):
        print("")
        if(len(eomList) == 0):
            print("No equations of motion were found (i.e. everything cancelled out).")
        else:
            for i in range(0, len(eomList), 1):
                sym.pprint(sym.Eq(0, eomList[i]))

    if(texOut != "n"):
        for i in range(0, len(eomList), 1):
            oup.addTexEq(0, eomList[i])

        oup.previewTex(texOut[1:])


# The main function for finding EoMs.
def runEoMs():
    # What do you want to do?
    print("\nContinuous ('c'), Semi-Discrete ('d'), or Fully-Discrete ('f')?")
    choice = inp.inpInList(["c", "d", "f"])

    # Read in the appropriate matrices.
    xMat = inp.MatrixWrapper()
    tMat = inp.MatrixWrapper()

    if(choice == 'c'):
        inp.parseMatrix(xMat, "U(x, t)")
        inp.parseMatrix(tMat, "V(x, t)")
    elif(choice == 'd'):
        inp.parseMatrix(xMat, "L(n, t)")
        inp.parseMatrix(tMat, "A(n, t)")
    elif(choice == 'f'):
        inp.parseMatrix(xMat, "L(n, m)")
        inp.parseMatrix(tMat, "A(n, m)")

    # Try to simplify the results (for debugging's sake).
    print("\nDo you want the computer to try to remove repeated expressions ('y'/'n')?\n  NOTE: This is just here for debugging/safety.")
    cutDupes = (inp.inpInList(["y", "n"]) == "y")

    # Would you like LaTeX output?
    print("\nWould you like to try to view the output with LaTeX formatting ('y'/'n'/'save')?")
    texify = inp.inpInList(["y", "n", "save"])
    if(texify == "save"):
        print("\nPlease enter the file name to use.")
        texify = "s" + inp.getInp()

    printEoMs(xMat.getWhole(), tMat.getWhole(), choice, True, texify, cutDupes)

