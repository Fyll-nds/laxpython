import sympy as sym
from sympy.parsing.sympy_parser import parse_expr

import Common.InputParsing as inp
from Common.OutputStyling import boxedOup
from Alg.KMatTest import testKMat
from Alg.YangBaxter import testYBE, testCYBE
import Alg.PoissonBrackets as PBs

# The code that drops you down to the function that you requets (with the inputted matrices in tow).
def runAlg():
    # What do you want to do?
    print("\nNow what would you like to do? Your choices are:")
    print("  - Find the Poisson Brackets for a discrete system. ('d')")
    print("  - Find the Poisson Brackets for a continuous system. ('c')")
    print("  - Check if a boundary K-matrix is acceptable. ('k')")
    print("  - Check that an R-matrix satisfies the Yang-Baxter equation. ('y')")
    print("  - Check that an r-matrix satisfies the classcial Yang-Baxter equation. ('r')")
    choice = inp.inpInList(["d", "c", "k", "y", "r"])

    # Read in the appropriate matrix.
    mainMat = inp.MatrixWrapper()
    if(choice == 'd'):
        inp.parseMatrix(mainMat, "L(n)")
    elif(choice == 'c'):
        inp.parseMatrix(mainMat, "U")
    elif(choice == 'k'):
        inp.parseMatrix(mainMat, "K")

    # Input the R-/r-matrix.
    rMat = inp.parse4Matrix("R" if (choice == "y") else "r")
    print("\nIs the spectral parameter additive ('+') or multiplicative ('*')?")
    spAdd = (inp.inpInList(["+", "*"]) == "+")

    # If you're doing PBs, do you want a TeXed output?
    texify = False
    if(choice == "d" or choice == "c"):
        print("\nDo you want to try viewing the Poisson brackets with LaTeX formatting ('y'/'n'/'save')?")
        texify = inp.inpInList(["y", "n", "save"])

        if(texify == "save"):
            print("\nPlease enter the file name to use.")
            texify = "s" + inp.getInp()

    # Now fall through to the appropriate function.
    if(choice == 'd'):
        PBs.printPBs(True, mainMat.getWhole(), PBs.quadAlgRHS(mainMat.getWhole(), rMat, spAdd), True, texify)
    elif(choice == 'c'):
        PBs.printPBs(False, mainMat.getWhole(), PBs.linAlgRHS(mainMat.getWhole(), rMat, spAdd), True, texify)
    elif(choice == 'k'):
        if(testKMat(mainMat.getWhole(), rMat, spAdd)):
            boxedOup("This K-matrix indeed satisfies the appropriate relation!")
        else:
            boxedOup("This K-matrix does not satisfy the appropriate relation.")
    elif(choice == 'y'):
        if(testYBE(rMat, spAdd)):
            boxedOup("This R-matrix does satisfy the Yang-Baxter equation!")
        else:
            boxedOup("This R-matrix does not satisfy the Yang-Baxter equation.")
    elif(choice == 'r'):
        if(testCYBE(rMat, spAdd)):
            boxedOup("This r-matrix does satisfy the classical Yang-Baxter equation!")
        else:
            boxedOup("This r-matrix does not satisfy the classical Yang-Baxter equation.")

