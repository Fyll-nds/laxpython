import sympy as sym

u, u_2 = sym.symbols("u u_2")

# Takes an R-matrix, and checks if it satisfies the Yang-Baxter equation.
def testYBE(rMat, spAdd):
    # Define your terms.
    R_ab = rMat.subs(u, ((u - u_2) if spAdd else (u / u_2)))
    R_ac = rMat
    R_bc = rMat.subs(u, u_2)

    # R_ab R_bc R_ac - R_ac R_bc R_ab = 0
    # 8x8 matrix, so there are 64 = 2^6 entries to check.
    for i1 in range(0, 2, 1):
        for i2 in range(0, 2, 1):
            for i3 in range(0, 2, 1):
                for i4 in range(0, 2, 1):
                    for i5 in range(0, 2, 1):
                        for i6 in range(0, 2, 1):
                            lhs = 0

                            # Inside each of those entries, there are 3 contracted indices (one for each tensor space).
                            for j1 in range(0, 2, 1):
                                for j2 in range(0, 2, 1):
                                    for j3 in range(0, 2, 1):
                                        lhs += R_ab[i1 + (2 * i2), j1 + (2 * j2)] * R_ac[j1 + (2 * i3), i4 + (2 * j3)] * R_bc[j2 + (2 * j3), i5 + (2 * i6)]
                                        lhs -= R_bc[i2 + (2 * i3), j1 + (2 * j2)] * R_ac[i1 + (2 * j2), j3 + (2 * i6)] * R_ab[j3 + (2 * j1), i4 + (2 * i5)]

                            # If this isn't 0, then at least one of the entries isn't okay, so just quit now.
                            if(sym.simplify(lhs) != 0):
                                return False

    # If none of the entries were non-zero, then everything is okay.
    return True


# Takes an r-matrix, and checks if it satisfies the classical Yang-Baxter equation.
def testCYBE(rMat, spAdd):
    # Define your terms.
    r_ab = rMat.subs(u, ((u - u_2) if spAdd else (u / u_2)))
    r_ac = rMat
    r_bc = rMat.subs(u, u_2)

    # [r_ab, r_ac] + [r_ab, r_bc] + [r_ab, r_ac] = 0
    # 8x8 matrix, so there are 64 = 2^6 entries to check.
    for i1 in range(0, 2, 1):
        for i2 in range(0, 2, 1):
            for i3 in range(0, 2, 1):
                for i4 in range(0, 2, 1):
                    for i5 in range(0, 2, 1):
                        for i6 in range(0, 2, 1):
                            lhs = 0

                            # Inside each of those entries, there is 1 contracted index (due to there only being one overlapping tensor space).
                            for j in range(0, 2, 1):
                                lhs += r_ab[i1 + (2 * i3), j + (2 * i4)] * r_ac[j + (2 * i5), i2 + (2 * i6)]
                                lhs -= r_ac[i1 + (2 * i5), j + (2 * i6)] * r_ab[j + (2 * i3), i2 + (2 * i4)]
                                lhs += r_ab[i1 + (2 * i3), i2 + (2 * j)] * r_bc[j + (2 * i5), i4 + (2 * i6)]
                                lhs -= r_bc[i3 + (2 * i5), j + (2 * i6)] * r_ab[i1 + (2 * j), i2 + (2 * i4)]
                                lhs += r_ac[i1 + (2 * i5), i2 + (2 * j)] * r_bc[i3 + (2 * j), i4 + (2 * i6)]
                                lhs -= r_bc[i3 + (2 * i5), i4 + (2 * j)] * r_ac[i1 + (2 * j), i2 + (2 * i6)]

                            # If this isn't 0, then at least one of the entries isn't okay, so just quit now.
                            if(sym.simplify(lhs) != 0):
                                return False

    # If every element was 0, then this is all good.
    return True
