import sympy as sym
import Common.InputParsing as inp
import Common.OutputStyling as oup
import Common.MatrixSeries as MatrixSeries

u, x, t, n = sym.symbols("u x t n")

# Print out the BT relations (without any solving having been done).
def printBTRels(coord, eqnMat, texify):
    # Say what you're printing out.
    oup.printSepLine()
    print("\tThe " + coord + " Equations:")
    if(texify):
        oup.addTexSepLine()
        oup.addTexText("\\qquad\\qquad The " + coord + " Equations:")

    # Then print it out.
    for i in range(0, len(eqnMat.degs), 1):
        for j in range(0, 4, 1):
            if(eqnMat.mats[i][j % 2, j // 2] != 0):
                sym.pprint(sym.Eq(0, eqnMat.mats[i][j % 2, j // 2], evaluate = False))

                if(texify):
                    oup.addTexEq(0, eqnMat.mats[i][j % 2, j // 2])


# A class containing a field.
# Nothing to see here.
"""class myField:
    def __init__(self, symb, pars):
        self.symb = symb
        self.pars = pars
        self.diffs = [0 for (i in range(0, len(pars), 1))]


    # Differentiate this field with respect to the given parameter.
    def diff(self, par):
        if(par in self.pars):
            self.diffs[self.pars.index(par)] += 1"""

"""
b
c
[[u/2, r(x, t)], [q(x, t), -u/2]]
y
[[u/2, s(x, t)], [p(x, t), -u/2]]
y
[[(u ** 2) - (r(x, t) * q(x, t)), (u * r(x, t)) + diff(r(x, t), x)], [(u * q(x, t)) - diff(q(x, t), x), q(x, t) * r(x, t)]]
y
[[(u ** 2) - (s(x, t) * p(x, t)), (u * s(x, t)) + diff(s(x, t), x)], [(u * p(x, t)) - diff(p(x, t), x), p(x, t) * s(x, t)]]
y
[[u + a(x, t), b(x, t)], [c(x, t), u - a(x, t)]]
y

"""

# The main function.
def runBTs():
    oup.boxedOup("This only prints out the expressions for now. Sorry.")

    # Discrete or Continuous?
    print("\nIs the space component of the Lax pair discrete ('d') or continuous ('c')?")
    xCts = (inp.inpInList(["d", "c"]) == "c")

    # Read in the matrices (stored as [space1, space2, time1, time2, Darboux]).
    mats = [MatrixSeries.MatrixSeries(), MatrixSeries.MatrixSeries(), MatrixSeries.MatrixSeries(), MatrixSeries.MatrixSeries(), MatrixSeries.MatrixSeries()]
    inp.parseMatrix(mats[0], ("U" if xCts else "L(n)"))
    inp.parseMatrix(mats[1], ("tilde'd U" if xCts else "tilde'd L(n)"))
    inp.parseMatrix(mats[2], "V")
    inp.parseMatrix(mats[3], "tilde'd V")
    inp.parseMatrix(mats[4], ("M" if xCts else "M(n)"))

    # Unwrap all of the matrices, to save time later.
    for i in range(0, len(mats), 1):
        mats[i] = mats[i].getWhole()

    print("\nDo you want to try to view the output with LaTeX formatting ('y'/'n'/'save')?")
    texify = inp.inpInList(["y", "n", "save"])
    if(texify == "save"):
        print("\nPlease enter the file name to use.")
        texify = "s" + inp.getInp()

    # Now generate the expressions, and store them in btEqs.
    btEqs = MatrixSeries.MatrixSeries()
    holdMat = sym.zeros(2, 2)

    # First the space equations.
    if(xCts):
        # Differentiate the Darboux matrix term by term.
        for i in range(0, 4, 1):
            holdMat[i % 2, i // 2] = sym.diff(mats[4][i % 2, i // 2], x)

        btEqs.setMat(holdMat - (mats[1] * mats[4]) + (mats[4] * mats[0]))
    else:
        btEqs.setMat((mats[1] * mats[4]) - (mats[4].subs(n, n + 1) * mats[0]))

    printBTRels("Space", btEqs, texify != "n")

    # Then the time equations.
    for i in range(0, 4, 1):
        holdMat[i % 2, i // 2] = sym.diff(mats[4][i % 2, i // 2], t)

    btEqs.setMat(holdMat - (mats[3] * mats[4]) + (mats[4] * mats[2]))
    printBTRels("Time", btEqs, texify != "n")

    # Finally, show the TeX if requested.
    if(texify != "n"):
        oup.previewTex(texify[1:])

