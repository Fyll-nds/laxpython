import tkinter as tk
from sympy import zeros
from GUI.CommonGUI import addHSep, add2x2Matrix, add4x4Matrix, parseGUIMatrix
from Common.OutputStyling import clearTex

from Cts.CtsLax import initCts, printCtsResults
from Common.MatrixSeries import MatrixSeries

# A wrapper for the frame containing the stuff for calculating continuous results.
class CtsFrame:
    def __init__(self, msgFunc):
        self.msgFunc = msgFunc

        self.frame = tk.Frame(bd = 2, relief = tk.SUNKEN)
        self.frame.grid_columnconfigure(0, weight = 1)

        self.doHams = tk.IntVar()
        self.doHams.set(0)

        self.doVMats = tk.IntVar()
        self.doVMats.set(0)
        self.rMat = None

        self.doOpenBCs = tk.IntVar()
        self.doOpenBCs.set(0)
        self.KMats = []
        self.addParam = tk.IntVar()
        self.addParam.set(True)

        self.lim = tk.StringVar()
        self.lim.set("0")

        self.saveOup = tk.IntVar()
        self.saveOup.set(False)

        # U-matrix entry space.
        add2x2Matrix(self.frame, "U(x)", 1, 0)

        # Hams and/or V_matrices (leave a row blank in case you need to enter an r-matrix)?
        addHSep(self.frame, 2)
        hamInpFrame = tk.Frame(self.frame)
        hamInpFrame.grid(row = 3, column = 0, sticky = "ew")
        tk.Checkbutton(hamInpFrame, text = "Hamiltonians", variable = self.doHams).grid(row = 0, column = 0)
        tk.Checkbutton(hamInpFrame, text = "V-Matrices", variable = self.doVMats, command = self.toggleRMat).grid(row = 0, column = 1)

        # Open BCs (leave two rows blank in case of K-matrices)?
        addHSep(self.frame, 5)
        tk.Checkbutton(self.frame, text = "Open Boundary Conditions", variable = self.doOpenBCs, command = self.toggleKMats).grid(row = 6, column = 0, sticky = "w")

        # Additive or Multiplicative Spectral Parameter?
        addHSep(self.frame, 9)
        spType = tk.Frame(self.frame)
        spType.grid(row = 10, column = 0, sticky = "ew")
        tk.Radiobutton(spType, text = "Additive Parameter", variable = self.addParam, value = True).grid(row = 0, column = 0)
        tk.Radiobutton(spType, text = "Multiplicative Parameter", variable = self.addParam, value = False).grid(row = 0, column = 1)

        # Lambda limit.
        addHSep(self.frame, 11)
        limInpFrame = tk.Frame(self.frame)
        limInpFrame.grid(row = 12, column = 0, sticky = "ew")
        tk.Label(limInpFrame, text = "Limit: u -> ").grid(row = 0, column = 0)
        tk.OptionMenu(limInpFrame, self.lim, "0", "inf").grid(row = 0, column = 1)

        # How many do you want?
        addHSep(self.frame, 13)
        numInpFrame = tk.Frame(self.frame)
        numInpFrame.grid(row = 14, column = 0, sticky = "ew")
        tk.Label(numInpFrame, text = "How many terms do you want: ").grid(row = 0, column = 0)
        self.numInp = tk.Entry(numInpFrame)
        self.numInp.insert(0, "1")
        self.numInp.grid(row = 0, column = 1)

        # Save the output?
        addHSep(self.frame, 15)
        saveOpt = tk.Frame(self.frame)
        saveOpt.grid(row = 16, column = 0, sticky = "ew")
        tk.Checkbutton(saveOpt, text = "Save Output?", variable = self.saveOup).grid(row = 0, column = 0)
        tk.Entry(saveOpt).grid(row = 0, column = 1)

        # The most important widget.
        addHSep(self.frame, 17)
        tk.Button(self.frame, text = "Run", command = self.runWrapper).grid(row = 18, column = 0, sticky = "ew")


    # Toggle the section for inputting the r-matrix.
    def toggleRMat(self):
        if(self.doVMats.get() == 0):
            self.rMat.grid_remove()
        else:
            # If the r-matrix hasn't been opened yet, create it.
            if(self.rMat == None):
                add4x4Matrix(self.frame, "r", 4, 0)
                self.rMat = self.frame.grid_slaves(4, 0)[0]
            else:
                self.rMat.grid(row = 4, column = 0)


    # Toggle the section for inputting the K-matrices.
    def toggleKMats(self):
        if(self.doOpenBCs.get() == 0):
            self.KMats[0].grid_remove()
            self.KMats[1].grid_remove()
        else:
            # If the K-matrices haven't been opened yet, create them.
            if(len(self.KMats) == 0):
                # Put in the matrix input areas.
                add2x2Matrix(self.frame, "K(+)", 7, 0)
                add2x2Matrix(self.frame, "K(-)", 8, 0)
                self.KMats = [self.frame.grid_slaves(7, 0)[0], self.frame.grid_slaves(8, 0)[0]]
            else:
                self.KMats[0].grid(row = 7, column = 0)
                self.KMats[1].grid(row = 8, column = 0)


    # A wrapper to wrap the run() function in, so that it can tell you while it is running.
    def runWrapper(self):
        tk.Label(self.frame, text = "The code is being run. Please be patient.").grid(row = 19, column = 0)
        self.msgFunc("")

        self.frame.update()
        self.run()
        self.frame.grid_slaves(19, 0)[0].grid_remove()


    # Actually try running the code with the inputted info.
    def run(self):
        # Make sure the user is actually trying to do something.
        if(not self.doHams.get() and not self.doVMats.get()):
            self.msgFunc("Please tick at least one of \"Hamiltonians\" or \"V-Matrices\".", col = "black")
            return

        # Convert all of the inputs into SymPy objects (or whatever).
        # [UMat, rMat, KMats[0], KMats[1]]
        inpMats = [MatrixSeries(), zeros(4, 4), MatrixSeries(), MatrixSeries()]
        holdInpMat = parseGUIMatrix(self.frame.grid_slaves(1, 0)[0], 2)
        if(holdInpMat == None):
            self.msgFunc("Failed to parse the U-matrix.", col = "Black")
            return
        else:
            inpMats[0].setMat(holdInpMat)

        # Fetch the r-matrix if necessary.
        if(self.doVMats.get()):
            inpMats[1] = parseGUIMatrix(self.frame.grid_slaves(4, 0)[0], 4)
            if(inpMats[1] == None):
                self.msgFunc("Failed to parse the r-matrix.", col = "Black")
                return

        # Fetch the K-matrices if necessary.
        if(self.doOpenBCs.get()):
            holdInpMat = parseGUIMatrix(self.frame.grid_slaves(7, 0)[0], 2)
            if(holdInpMat == None):
                self.msgFunc("Failed to parse the K(+)-matrix.", col = "Black")
                return
            else:
                inpMats[2].setMat(holdInpMat)

            holdInpMat = parseGUIMatrix(self.frame.grid_slaves(8, 0)[0], 2)
            if(holdInpMat == None):
                self.msgFunc("Failed to parse the K(-)-matrix.", col = "Black")
                return
            else:
                inpMats[3].setMat(holdInpMat)

        # [openBCs, additive parameter, rMat type, invLim, limit factor]
        inpNonMats = [self.doOpenBCs.get(), self.addParam.get(), "*", self.lim.get(), 1.0]

        # Before running, check that the number of results is suitable.
        numTerms = 0
        try:
            numTerms = int(self.numInp.get())
        except ValueError:
            self.msgFunc("Failed to parse the number of terms.", col = "black")
            return

        # If you want to save, check for that.
        oupType = "y"
        if(self.saveOup.get()):
            fileName = self.frame.grid_slaves(16, 0)[0].grid_slaves(0, 1)[0].get()
            if(fileName == ""):
                self.msgFunc("Please input a file name to save to.", col = "black")
                return
            else:
                oupType = "s" + fileName

        # Call the functions that run the code.
        initCts(inpMats, inpNonMats)
        printCtsResults(numTerms, [self.doHams.get(), self.doVMats.get(), False, False], False, oupType)

        if(oupType[0] == "s"):
            self.msgFunc("The output has been successfully saved!", col = "green")

        # Now that you're out, clear the TeX for next time.
        clearTex()

