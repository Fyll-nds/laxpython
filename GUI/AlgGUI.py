import tkinter as tk
from GUI.CommonGUI import addHSep, add2x2Matrix, add4x4Matrix, parseGUIMatrix
from Common.OutputStyling import clearTex

from Alg.PoissonBrackets import printPBs, linAlgRHS, quadAlgRHS
from Alg.KMatTest import testKMat
from Alg.YangBaxter import testYBE, testCYBE

# A wrapper for the frame containing the algebra bits.
class AlgFrame:
    def __init__(self, msgFunc):
        self.msgFunc = msgFunc

        self.frame = tk.Frame(bd = 2, relief = tk.SUNKEN)
        self.frame.grid_columnconfigure(0, weight = 1)

        self.gotoAlg = tk.IntVar()
        self.gotoAlg.set(0)
        self.currentOpt = 0

        self.addParam = tk.IntVar()
        self.addParam.set(True)

        self.saveOup = tk.IntVar()
        self.saveOup.set(False)
        self.saveFrame = tk.Frame(self.frame)

        self.inpMatFrames = [[], [], [], [], []]
        self.inpMatNames = [["U(x)", "r"], ["L(n)", "r"], ["K", "r"], ["R", ""], ["r", ""]]

        # Fill the frame.
        tk.Radiobutton(self.frame, text = "Continuous Poisson Brackets", variable = self.gotoAlg, value = 0, command = self.setOption).grid(row = 0, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Discrete Poisson Brackets", variable = self.gotoAlg, value = 1, command = self.setOption).grid(row = 1, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Check Boundary K-Matrix", variable = self.gotoAlg, value = 2, command = self.setOption).grid(row = 2, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Check R-Matrix in the YBE", variable = self.gotoAlg, value = 3, command = self.setOption).grid(row = 3, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Check r-Matrix in the cYBE", variable = self.gotoAlg, value = 4, command = self.setOption).grid(row = 4, column = 0, sticky = "w")

        addHSep(self.frame, 5)

        # Add the matrix inputting stuff for the default (cts PBs).
        add2x2Matrix(self.frame, self.inpMatNames[0][0], 6, 0)
        add4x4Matrix(self.frame, self.inpMatNames[0][1], 7, 0)
        self.inpMatFrames[0] = [self.frame.grid_slaves(6, 0)[0], self.frame.grid_slaves(7, 0)[0]]

        # What type is the spectral parameter?
        addHSep(self.frame, 8)
        spType = tk.Frame(self.frame)
        spType.grid(row = 9, column = 0, sticky = "ew")
        tk.Radiobutton(spType, text = "Additive Parameter", variable = self.addParam, value = True).grid(row = 0, column = 0)
        tk.Radiobutton(spType, text = "Multiplicative Parameter", variable = self.addParam, value = False).grid(row = 0, column = 1)

        # Save the output?
        addHSep(self.frame, 10)
        self.saveFrame.grid(row = 11, column = 0, sticky = "ew")
        tk.Checkbutton(self.saveFrame, text = "Save Output?", variable = self.saveOup).grid(row = 0, column = 0)
        tk.Entry(self.saveFrame).grid(row = 0, column = 1)

        # The all-important button.
        addHSep(self.frame, 12)
        tk.Button(self.frame, text = "Run", command = self.runWrapper).grid(row = 13, column = 0, sticky = "ew")


    # Switch which lot of algebra stuff is visible.
    def setOption(self):
        # Remove the old ones.
        self.inpMatFrames[self.currentOpt][0].grid_remove()
        self.inpMatFrames[self.currentOpt][1].grid_remove()

        # If the option you're leaving had the save option, hide it.
        if(self.currentOpt == 0 or self.currentOpt == 1):
            self.saveFrame.grid_remove()

        self.currentOpt = self.gotoAlg.get()

        # If the new stuff hasn't been set up yet, set it up.
        if(len(self.inpMatFrames[self.currentOpt]) == 0):
            for i in range(0, 2, 1):
                # R/r-matrices are 4x4.
                if(self.charToDim(self.inpMatNames[self.currentOpt][i]) == 4):
                    add4x4Matrix(self.frame, self.inpMatNames[self.currentOpt][i], 6 + i, 0)
                # Everything else is 2x2.
                elif(self.charToDim(self.inpMatNames[self.currentOpt][i]) == 2):
                    add2x2Matrix(self.frame, self.inpMatNames[self.currentOpt][i], 6 + i, 0)
                # ...Except the case that doesn't exist.
                else:
                    tk.Frame(self.frame).grid(row = 6 + i, column = 0)

            self.inpMatFrames[self.currentOpt] = [self.frame.grid_slaves(6, 0)[0], self.frame.grid_slaves(7, 0)[0]]
        # Else just put the new ones down.
        else:
            self.inpMatFrames[self.currentOpt][0].grid(row = 6, column = 0)
            self.inpMatFrames[self.currentOpt][1].grid(row = 7, column = 0)

        # If the option that's been chosen wants the save option, bring it back.
        if(self.currentOpt == 0 or self.currentOpt == 1):
            self.saveFrame.grid(row = 11, column = 0, sticky = "ew")


    # Converts a character (well, string) to the appropriate matrix's dimension.
    def charToDim(self, ch):
        if(ch == "R" or ch == "r"):
            return 4
        elif(ch == ""):
            return 0
        else:
            return 2


    # A wrapper to wrap the run() function in, so that it can tell you while it is running.
    def runWrapper(self):
        tk.Label(self.frame, text = "The code is being run. Please be patient.").grid(row = 14, column = 0)
        self.msgFunc("")

        self.frame.update()
        self.run()
        self.frame.grid_slaves(14, 0)[0].grid_remove()


    # Actually tries running the code with the inputted info.
    def run(self):
        # Read in the first matrix, and check that it's okay.
        parsedMats = [parseGUIMatrix(self.inpMatFrames[self.currentOpt][0], self.charToDim(self.inpMatNames[self.currentOpt][0]))]
        if(parsedMats[0] == None):
            self.msgFunc("Failed to parse the " + self.inpMatNames[self.currentOpt][0] + "-matrix.", col = "black")
            return

        # Then read in the second matrix (if necessary), and check that it is also okay.
        if(self.inpMatNames[self.currentOpt][1] != ""):
            parsedMats.append(parseGUIMatrix(self.inpMatFrames[self.currentOpt][1], self.charToDim(self.inpMatNames[self.currentOpt][1])))

            if(parsedMats[1] == None):
                self.msgFunc("Failed to parse the " + self.inpMatNames[self.currentOpt][1] + "-matrix.", col = "black")
                return

        # If you want to save, check for that.
        oupType = "y"
        if((self.currentOpt == 0 or self.currentOpt == 1) and self.saveOup.get()):
            fileName = self.frame.grid_slaves(11, 0)[0].grid_slaves(0, 1)[0].get()
            if(fileName == ""):
                self.msgFunc("Please input a file name to save to.", col = "black")
                return
            else:
                oupType = "s" + fileName

        # Now that you have all of the matrices, pass them through to the appropriate function.
        # Continuous Poisson Brackets
        if(self.currentOpt == 0):
            printPBs(False, parsedMats[0], linAlgRHS(parsedMats[0], parsedMats[1], self.addParam.get()), False, oupType)
        # Discrete Poisson Brackets
        elif(self.currentOpt == 1):
            printPBs(True, parsedMats[0], quadAlgRHS(parsedMats[0], parsedMats[1], self.addParam.get()), False, oupType)
        # Boundary K-Matrices
        elif(self.currentOpt == 2):
            if(testKMat(parsedMats[0], parsedMats[1], self.addParam.get())):
                self.msgFunc("The given K-matrix satisfies the appropriate relation!", col = "green")
            else:
                self.msgFunc("The given K-matrix does not satisfy the appropriate relation.", col = "red")
        # Yang-Baxter Equation
        elif(self.currentOpt == 3):
            if(testYBE(parsedMats[0], self.addParam.get())):
                self.msgFunc("The given R-matrix satisfies the Yang-Baxter equation!", col = "green")
            else:
                self.msgFunc("The given R-matrix does not satisfy the Yang-Baxter equation.", col = "red")
        # Classical Yang-Baxter Equation
        elif(self.currentOpt == 4):
            if(testCYBE(parsedMats[0], self.addParam.get())):
                self.msgFunc("The given r-matrix satisfies the classical Yang-Baxter equation!", col = "green")
            else:
                self.msgFunc("The given r-matrix does not satisfy the classical Yang-Baxter equation.", col = "red")

        if((self.currentOpt == 0 or self.currentOpt == 1) and oupType[0] == "s"):
            self.msgFunc("The output has been successfully saved!", col = "green")

        # Now that you're out, clear the TeX for next time.
        clearTex()

