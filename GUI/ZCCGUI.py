import tkinter as tk
from GUI.CommonGUI import addHSep, add2x2Matrix, parseGUIMatrix
from Common.OutputStyling import clearTex
from Alg.ZCC import printEoMs

# A wrapper for the frame used to calculate the equations of motion from the ZCC.
class ZCCFrame:
    def __init__(self, msgFunc):
        self.msgFunc = msgFunc

        self.frame = tk.Frame(bd = 2, relief = tk.SUNKEN)
        self.frame.grid_columnconfigure(0, weight = 1)

        self.saveOup = tk.IntVar()
        self.saveOup.set(False)

        self.discNum = tk.IntVar()
        self.discNum.set(0)
        self.oldDiscNum = 0
        self.inpMatFrames = [[], [], []]

        # Fill the frame.
        tk.Radiobutton(self.frame, text = "Continuous", variable = self.discNum, value = 0, command = self.setOption).grid(row = 0, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Semi-Discrete", variable = self.discNum, value = 1, command = self.setOption).grid(row = 1, column = 0, sticky = "w")
        tk.Radiobutton(self.frame, text = "Fully-Discrete", variable = self.discNum, value = 2, command = self.setOption).grid(row = 2, column = 0, sticky = "w")

        addHSep(self.frame, 3)

        # Add the matrix inputting bits.
        add2x2Matrix(self.frame, "U(x, t)", 4, 0)
        add2x2Matrix(self.frame, "V(x, t)", 5, 0)
        self.inpMatFrames[0] = [self.frame.grid_slaves(4, 0)[0], self.frame.grid_slaves(5, 0)[0]]

        # Save the output?
        addHSep(self.frame, 6)
        saveOpt = tk.Frame(self.frame)
        saveOpt.grid(row = 7, column = 0, sticky = "ew")
        tk.Checkbutton(saveOpt, text = "Save Output?", variable = self.saveOup).grid(row = 0, column = 0)
        tk.Entry(saveOpt).grid(row = 0, column = 1)

        # Add the "run" button.
        addHSep(self.frame, 8)
        tk.Button(self.frame, text = "Run", command = self.runWrapper).grid(row = 9, column = 0, sticky = "ew")


    # Switch between the discrete ZCCs and the continuous ZCC.
    def setOption(self):
        # Remove the old ones.
        self.inpMatFrames[self.oldDiscNum][0].grid_remove()
        self.inpMatFrames[self.oldDiscNum][1].grid_remove()

        # If the matrices aren't set up yet, set them up.
        if(len(self.inpMatFrames[self.discNum.get()]) == 0):
            add2x2Matrix(self.frame, "L(n, " + ("t" if (self.discNum.get() == 1) else "m") + ")", 4, 0)
            add2x2Matrix(self.frame, "A(n, " + ("t" if (self.discNum.get() == 1) else "m") + ")", 5, 0)
            self.inpMatFrames[self.discNum.get()] = [self.frame.grid_slaves(4, 0)[0], self.frame.grid_slaves(5, 0)[0]]
        # Else just put the new ones down.
        else:
            self.inpMatFrames[self.discNum.get()][0].grid(row = 4, column = 0)
            self.inpMatFrames[self.discNum.get()][1].grid(row = 5, column = 0)

        # Make a note of what the current option is.
        self.oldDiscNum = self.discNum.get()


    # A wrapper to wrap the run() function in, so that it can tell you while it is running.
    def runWrapper(self):
        tk.Label(self.frame, text = "The code is being run. Please be patient.").grid(row = 10, column = 0)
        self.msgFunc("")

        self.frame.update()
        self.run()
        self.frame.grid_slaves(10, 0)[0].grid_remove()


    # Try to run the program with the inputted data.
    def run(self):
        # Parse the matrices.
        mats = [parseGUIMatrix(self.frame.grid_slaves(4, 0)[0], 2), parseGUIMatrix(self.frame.grid_slaves(5, 0)[0], 2)]

        # Make sure that they're okay.
        if(mats[0] == None):
            self.msgFunc("Unable to parse the " + ("U" if (self.oldDiscNum == 0) else "L") + "-matrix.", col = "black")
            return
        if(mats[1] == None):
            self.msgFunc("Unable to parse the " + ("V" if (self.oldDiscNum == 0) else "A") + "-matrix.", col = "black")
            return

        # If you want to save, check for that.
        oupType = "y"
        if(self.saveOup.get()):
            fileName = self.frame.grid_slaves(7, 0)[0].grid_slaves(0, 1)[0].get()
            if(fileName == ""):
                self.msgFunc("Please input a file name to save to.", col = "black")
                return
            else:
                oupType = "s" + fileName

        # Print your newfound results (after you find them).
        printEoMs(mats[0], mats[1], (["c", "d", "f"])[self.oldDiscNum], False, oupType)

        if(oupType[0] == "s"):
            self.msgFunc("The output has been successfully saved!", col = "green")

        # Now that you're out, clear the TeX for next time.
        clearTex()

