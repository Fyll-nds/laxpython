import tkinter as tk
from webbrowser import open_new

# Opens the address given in the third tag (awkward, but eh).
def openWebAddress(event):
    open_new(event.widget.tag_names(tk.CURRENT)[2])

# Changes the cursor to and from being a hand cursor (for hyperlinks).
def showHandCursor(event):
    event.widget.config(cursor = "hand2")
def showArrowCursor(event):
    event.widget.config(cursor = "arrow")

# A wrapper for the frame containing the credits. Very important! :D
class CreditFrame:
    def __init__(self, frameWidth):
        # Need to manually set the frame width, as the Text environment keeps trying to edit it otherwise.
        borderWidth = 2
        self.frame = tk.Frame(bd = borderWidth, relief = tk.SUNKEN, width = frameWidth - (2 * borderWidth))
        self.frame.grid_columnconfigure(0, weight = 1)
        self.frame.grid_propagate(False)

        # Set up the "tags" used to print the text.
        holdText = tk.Text(self.frame, wrap = tk.WORD, background = self.frame.cget("bg"), borderwidth = 0)
        holdText.grid(row = 0, column = 0, sticky = "ew")
        holdText.tag_configure("tag:center", justify = "center")
        holdText.tag_configure("tag:href", foreground = "blue", underline = True)
        holdText.tag_bind("tag:href", "<Button-1>", openWebAddress)
        holdText.tag_bind("tag:href", "<Enter>", showHandCursor)
        holdText.tag_bind("tag:href", "<Leave>", showArrowCursor)

        # Add all of the actual text.
        holdText.insert(tk.END, "This is a program for calculating things in the Lax formalism.\n\nThe code was written by Iain Findlay (iaf1@hw.ac.uk) in Python using the SymPy library, and this GUI was written using the TKinter library.\n\nFurther details and the latest build (if this isn't it) can be found at:\n", "tag:center")
        holdText.insert(tk.END, "https://bitbucket.org/Fyll-nds/laxpython/overview", ("tag:center", "tag:href", "https://bitbucket.org/Fyll-nds/laxpython/overview"))
        holdText.insert(tk.END, "\n\nThe official SymPy website is:\n", "tag:center")
        holdText.insert(tk.END, "www.sympy.org", ("tag:center", "tag:href", "www.sympy.org"))
        holdText.insert(tk.END, "\n\nI couldn't find an official TKinter page, but my (very short) Googling suggested that this might suffice:\n", "tag:center")
        holdText.insert(tk.END, "https://docs.python.org/2/library/tkinter.html", ("tag:center", "tag:href", "https://docs.python.org/2/library/tkinter.html"))

        # Stop the user from editing it.
        holdText.config(state = tk.DISABLED)

