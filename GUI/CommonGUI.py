import tkinter as tk
from sympy.parsing.sympy_parser import parse_expr
from sympy.parsing.sympy_tokenize import TokenError
from sympy import zeros

# A global variable (yay!) for holding the copied/pasted matrices.
GUI_storeMats = [[1, zeros(2, 2)], [1, zeros(4, 4)]]


# Functions for copying and pasting the stored matrix into the given location.
def copyMat(matFrame):
    global GUI_storeMats
    matDim = (2 if (matFrame.grid_size()[0] == 7) else 4)
    storedMat = GUI_storeMats[(matDim // 2) - 1]

    # Make a note of the prefactor.
    storedMat[0] = matFrame.grid_slaves(0, 1)[0].get()

    # Make a note of the matrix entries.
    for i in range(0, matDim, 1):
        for j in range(0, matDim, 1):
            storedMat[1][i, j] = matFrame.grid_slaves(1 + i, 4 + j)[0].get()

def pasteMat(matFrame):
    matDim = (2 if (matFrame.grid_size()[0] == 7) else 4)
    storedMat = GUI_storeMats[(matDim // 2) - 1]

    # Fill in the prefactor.
    matEntry = matFrame.grid_slaves(0, 1)[0]
    matEntry.delete(0, tk.END)
    matEntry.insert(0, storedMat[0])

    # Fill in the matrix entries.
    for i in range(0, matDim, 1):
        for j in range(0, matDim, 1):
            matEntry = matFrame.grid_slaves(1 + i, 4 + j)[0]
            matEntry.delete(0, tk.END)
            matEntry.insert(0, storedMat[1][i, j])


# Adds a horizontal separator.
def addHSep(frame, rowIn):
    tk.Frame(frame, height = 2, bd = 1, relief = tk.SUNKEN).grid(row = rowIn, column = 0, sticky = "ew")


# Creates a frame in the middle of the given grid location of mainFrame.
def addCenterFrame(mainFrame, rowIn, colIn):
    padFrame = tk.Frame(mainFrame)
    padFrame.grid(row = rowIn, column = colIn, sticky = "nesw")

    outFrame = tk.Frame(padFrame)
    outFrame.grid(row = 1, column = 1, rowspan = 2)

    # Make everything around the frame pad it.
    padFrame.grid_columnconfigure(0, weight = 1)
    padFrame.grid_columnconfigure(3, weight = 1)
    padFrame.grid_rowconfigure(0, weight = 1)
    padFrame.grid_rowconfigure(3, weight = 1)

    # Put buttons in to copy and paste the matrix.
    tk.Button(padFrame, text = "Copy", font = ("Helvetica", 8), padx = 1, pady = 0, command = lambda: copyMat(outFrame)).grid(row = 1, column = 2, sticky = "s")
    tk.Button(padFrame, text = "Paste", font = ("Helvetica", 8), padx = 0, pady = 0, command = lambda: pasteMat(outFrame)).grid(row = 2, column = 2, sticky = "n")

    return outFrame


# Adds an input region for a 2x2 matrix.
def add2x2Matrix(frame, name, rowIn, colIn):
    matFrame = addCenterFrame(frame, rowIn, colIn)

    # Print the name and brackets.
    tk.Label(matFrame, text = name + " = ").grid(row = 0, column = 0, rowspan = 4)
    tk.Label(matFrame, text = " * ").grid(row = 0, column = 2, rowspan = 6)
    tk.Label(matFrame, text = "╭\n│\n│\n╰").grid(row = 0, column = 3, rowspan = 4)
    tk.Label(matFrame, text = "╮\n│\n│\n╯").grid(row = 0, column = 6, rowspan = 4)

    # Draw some input boxes.
    prfInp = tk.Entry(matFrame, width = 0)
    prfInp.grid(row = 0, column = 1, rowspan = 6)
    prfInp.insert(0, "1")

    for i in range(0, 4, 1):
        holdInp = tk.Entry(matFrame, width = 0)
        holdInp.grid(row = 1 + (i // 2), column = 4 + (i % 2))
        holdInp.insert(0, "0")


# Adds an input region for a 4x4 matrix (with a prefactor).
def add4x4Matrix(frame, name, rowIn, colIn):
    matFrame = addCenterFrame(frame, rowIn, colIn)

    # Print the name and brackets.
    tk.Label(matFrame, text = name + " = ").grid(row = 0, column = 0, rowspan = 6)
    tk.Label(matFrame, text = " * ").grid(row = 0, column = 2, rowspan = 6)
    tk.Label(matFrame, text = "╭\n│\n│\n│\n│\n│\n╰").grid(row = 0, column = 3, rowspan = 6)
    tk.Label(matFrame, text = "╮\n│\n│\n│\n│\n│\n╯").grid(row = 0, column = 8, rowspan = 6)

    # Draw some input boxes.
    prfInp = tk.Entry(matFrame, width = 0)
    prfInp.grid(row = 0, column = 1, rowspan = 6)
    prfInp.insert(0, "1")

    for i in range(0, 4, 1):
        for j in range(0, 4, 1):
            holdInp = tk.Entry(matFrame, width = 0)
            holdInp.grid(row = 1 + i, column = 4 + j)
            holdInp.insert(0, "0")


# Converts a string from the GUI entry spaces into a sympy variable.
def parseGUIEntry(inpStr):
    # If the string is empty, assume they mean 0.
    if(inpStr == ""):
        return 0

    # Catch for if the input is unparseable.
    try:
        return parse_expr(inpStr)
    except (TokenError, SyntaxError, ValueError):
        return None


# Convert a frame containing a matrix (as constructed above) into a SymPy Matrix.
def parseGUIMatrix(frame, matDim):
    # First, remove the padding.
    frame = frame.grid_slaves(1, 1)[0]

    # Then parse the prefactor and check that it's safe to continue.
    prefactor = parseGUIEntry(frame.grid_slaves(0, 1)[0].get())
    if(prefactor == None):
        return None

    # Now parse each of the matrix entries, checking that they are all fine.
    outMat = zeros(matDim, matDim)
    for i in range(0, matDim, 1):
        for j in range(0, matDim, 1):
            holdInp = parseGUIEntry(frame.grid_slaves(1 + i, 4 + j)[0].get())

            if(holdInp == None):
                return None
            else:
                outMat[i, j] = prefactor * holdInp

    return outMat

