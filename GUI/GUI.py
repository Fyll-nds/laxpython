import tkinter as tk

import GUI.AlgGUI as AlgGUI
import GUI.CreditGUI as CreditGUI
import GUI.CtsGUI as CtsGUI
import GUI.DiscGUI as DiscGUI
import GUI.ZCCGUI as ZCCGUI

MIN_WIN_DIMS = [400, 500]
FRAME_PADDING = 3

# The class containing all of the information on the window.
class Window:
    # Initialise all of the variables that need initialising.
    def __init__(self, winName):
        self.win = tk.Tk()
        self.win.wm_title(winName)

        self.tabs = []
        self.currentTab = -1
        self.gotoTab = tk.IntVar()
        self.gotoTab.set(0)


    # Actually open up the window (locks the input here).
    def run(self):
        # Make sure that the window is the correct size.
        self.win.minsize(width = MIN_WIN_DIMS[0], height = MIN_WIN_DIMS[1])

        # Put "tabs" at the top.
        tk.Radiobutton(self.win, text = "Algebra", variable = self.gotoTab, value = 0, command = self.setFrame, indicatoron = False).grid(row = 0, column = 0, sticky = "ew")
        tk.Radiobutton(self.win, text = "Continuous", variable = self.gotoTab, value = 1, command = self.setFrame, indicatoron = False).grid(row = 0, column = 1, sticky = "ew")
        tk.Radiobutton(self.win, text = "Discrete", variable = self.gotoTab, value = 2, command = self.setFrame, indicatoron = False).grid(row = 0, column = 2, sticky = "ew")
        tk.Radiobutton(self.win, text = "Equations", variable = self.gotoTab, value = 3, command = self.setFrame, indicatoron = False).grid(row = 0, column = 3, sticky = "ew")
        tk.Radiobutton(self.win, text = "Credits", variable = self.gotoTab, value = 4, command = self.setFrame, indicatoron = False).grid(row = 0, column = 4, sticky = "ew")

        # Set up the frames.
        self.tabs.append(AlgGUI.AlgFrame(self.msgFunc))
        self.tabs.append(CtsGUI.CtsFrame(self.msgFunc))
        self.tabs.append(DiscGUI.DiscFrame(self.msgFunc))
        self.tabs.append(ZCCGUI.ZCCFrame(self.msgFunc))
        self.tabs.append(CreditGUI.CreditFrame(MIN_WIN_DIMS[0] - (2 * FRAME_PADDING)))

        # Put a bar at the bottom for error/output messages.
        self.oupLblCol = tk.Frame(self.win)
        self.oupLblCol.grid(row = 2, column = 0, columnspan = len(self.tabs), sticky = "ew")
        self.oupLblCol.grid_columnconfigure(0, weight = 1)
        colPad = tk.Frame(self.oupLblCol)
        colPad.grid(row = 0, column = 0, padx = 6, pady = 1, sticky = "ew")
        colPad.grid_columnconfigure(0, weight = 1)
        self.oupLblTxt = tk.Label(colPad, text = "")
        self.oupLblTxt.grid(row = 0, column = 0, sticky = "ew")

        # Put the front frame into the window (making sure that it fills it).
        self.setFrame()
        self.win.grid_rowconfigure(1, weight = 1)
        for i in range(0, 5, 1):
            self.win.grid_columnconfigure(i, weight = 1)

        # Run!
        self.win.mainloop()


    # Switches which tab you're on.
    def setFrame(self):
        # Don't try to remove a frame if one isn't set up.
        if(self.currentTab >= 0):
            self.tabs[self.currentTab].frame.grid_remove()

        self.currentTab = self.gotoTab.get()
        self.tabs[self.currentTab].frame.grid(row = 1, column = 0, columnspan = len(self.tabs), padx = FRAME_PADDING, pady = FRAME_PADDING, sticky = "nesw")

        # Remove any old error messages.
        self.msgFunc("")


    # A message for printing out messages.
    def msgFunc(self, msg, col = ""):
        # Set up the background if need be.
        if(col != ""):
            self.oupLblCol.config(bg = col)
        else:
            self.oupLblCol.config(bg = self.win.cget("bg"))

        # Print the message.
        self.oupLblTxt.config(text = msg)

