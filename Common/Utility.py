from sympy import Matrix

# Expand the given 2x2 matrix into a 4x4 matrix by tensoring it with the Identity (i.e. MxI, not IxM).
def tenProd_a(mat):
    return Matrix([[mat[0, 0], 0, mat[0, 1], 0], [0, mat[0, 0], 0, mat[0, 1]], [mat[1, 0], 0, mat[1, 1], 0], [0, mat[1, 0], 0, mat[1, 1]]])

# Do the opposite.
def tenProd_b(mat):
    return Matrix([[mat[0, 0], mat[0, 1], 0, 0], [mat[1, 0], mat[1, 1], 0, 0], [0, 0, mat[0, 0], mat[0, 1]], [0, 0, mat[1, 0], mat[1, 1]]])

# Trace out the _a tensor space.
def ptr_a(mat):
    return Matrix([[mat[0, 0] + mat[2, 2], mat[0, 1] + mat[2, 3]], [mat[1, 0] + mat[3, 2], mat[1, 1] + mat[3, 3]]])

# Trace out the _b tensor space.
def ptr_b(mat):
    return Matrix([[mat[0, 0] + mat[1, 1], mat[0, 2] + mat[1, 3]], [mat[2, 0] + mat[3, 1], mat[2, 2] + mat[3, 3]]])
