import sympy as sym

# Print out a separating line.
def printSepLine():
    print("\n}<" + ('=' * 75) + ">{\n")


# A fancy(ish) "boxed" output. Likely far more complicated than it ought to be.
def boxedOup(msg):
    print(" " + ("_" * 77))
    print("/" + (" " * 77) + "\\")

    # Wrap the message to keep it within 71 characters. Wrap it at a space, though, if possible.
    words = msg.split(" ")
    lines = []
    holdLine = words[0]
    for i in range(1, len(words) + 1, 1):
        if(i == len(words) or len(holdLine + " " + words[i]) > 71):
            lines.append(holdLine)

            if(i < len(words)):
                holdLine = words[i]
        else:
            holdLine += " " + words[i]

    # Put some fancy(ish) edges on.
    edges = []
    for i in range(1, len(lines) - 1, 4):
        if(i + 2 < len(lines) - 1):
            edges += [["\\_", "_/"], ["/ ", " \\"]]
        else:
            edges.append([">-", "-<"])

    # Actually print all of the text (with the fancy(ish) edges).
    midpoint = int((len(lines) - 1) / 2)
    for i in range(0, len(lines), 1):
        pad = (71 - len(lines[i])) / 2
        if(i == midpoint and len(lines) % 2):
            print(">-- " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + " --<")
        elif(i == midpoint and not (len(lines) % 2)):
            print("\\__ " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + " __/")
        elif(i == midpoint + 1 and not (len(lines) % 2)):
            print("/   " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + "   \\")
        elif(i < midpoint):
            print(edges[i][0] + "  " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + "  " + edges[i][1])
        else:
            hold = len(edges) + midpoint + 1 - (len(lines) % 2) - i
            if(edges[hold][0] == ">-"):
                print(edges[hold][0] + "  " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + "  " + edges[hold][1])
            elif(edges[hold][0] == "/ "):
                print(edges[hold - 1][0] + "  " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + "  " + edges[hold - 1][1])
            else:
                print(edges[hold + 1][0] + "  " + (" " * int(pad + 0.5)) + lines[i] + (" " * int(pad)) + "  " + edges[hold + 1][1])

    print("\\" + ("_" * 77) + "/\n")


# A string for storing the TeX in.
texStr = ""

# Append an equation to the TeX document.
def addTexEq(lhs, rhs):
    global texStr, texHeight
    if(isinstance(lhs, str)):
        texStr += sym.latex(sym.Eq(sym.Symbol(lhs), rhs, evaluate = False), mode = "equation*")
    else:
        texStr += sym.latex(sym.Eq(lhs, rhs, evaluate = False), mode = "equation*")


# Append some text to the TeX document.
def addTexText(text):
    global texStr
    texStr += text + " \\\\ \\- \\\\ \\- "


# Adds a horizontal divider to the TeX output.
def addTexSepLine():
    global texStr
    texStr += " \\hline \\\\ \\- "


# Clears the TeX that has been added so far.
def clearTex():
    global texStr
    texStr = ""


# Previews the TeX that has been supplied.
def previewTex(fileName):
    global texStr, texHeight

    # As pdf uses the "standalone" document class, we need to remove all of the equation environments.
    texStr = texStr.replace("\\begin{equation*}", " $\\displaystyle ").replace("\\end{equation*}", " $ \\\\ \\- \\\\ ")[:-3]

    # If they've asked to save it, save it into fileName. If they've not given an extension, default to a pdf.
    if(fileName != ""):
        fileBits = fileName.split(".")
        sym.preview(texStr + "\\end{tabular}", output = (fileBits[-1] if (len(fileBits) > 1) else "pdf"), viewer = "file", filename = fileName + ("" if (len(fileBits) > 1) else ".pdf"), preamble = "\\documentclass[border = 5px]{standalone}\\usepackage{amsmath, amsfonts}\\begin{document}\\begin{tabular}{l}")
        return

    # Try each of the available output options. First, try pdf, as that's the prettiest.
    try:
        sym.preview(texStr + "\\end{tabular}", output = "pdf", preamble = "\\documentclass[border = 5px]{standalone}\\usepackage{amsmath, amsfonts}\\begin{document}\\begin{tabular}{l}")
    except SystemError:
        try:
            sym.preview("\\begin{tabular}{l}" + texStr + "\\end{tabular}", output = "png")
        except ImportError:
            print("\nNo suitable viewers were found.")

