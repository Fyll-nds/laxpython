from sympy import Matrix, zeros, Poly, Symbol

u = Symbol("u")
 
# A class for holding and managing a matrix that has been split about powers of u.
class MatrixSeries:
    def __init__(self):
        self.degs = []
        self.mats = []
         
        self.uLow = 0
         
         
    # Given a whole matrix inpMat, split it about powers of u.
    def setMat(self, inpMat):
        # Clear out any old data.
        self.degs = []
        self.mats = []
         
        # Go term by term.
        for i in range(0, 4, 1):
            holdInp = Poly(inpMat[i // 2, i % 2], u, 1/u)
            cfs = holdInp.coeffs()
            dgs = holdInp.monoms()
             
            for j in range(0, len(cfs), 1):
                # Ignore terms with a 0 coefficient.
                if(cfs[j] != 0):
                    found = False
                    # If this order of u has already been added, just add yourself to that matrix.
                    for k in range(0, len(self.degs), 1):
                        if(dgs[j][0] - dgs[j][1] == self.degs[k]):
                            self.mats[k][i // 2, i % 2] += cfs[j]
                            found = True
                            break
 
                    # If it doesn't already exist, add it.
                    if(not found):
                        for k in range(0, len(self.degs), 1):
                            if(self.degs[k] > dgs[j][0] - dgs[j][1]):
                                self.degs.insert(k, dgs[j][0] - dgs[j][1])
                                self.mats.insert(k, zeros(2, 2))
                                self.mats[k][i // 2, i % 2] = cfs[j]
                                 
                                found = True
                                break
                                 
                        # If you didn't find anywhere to insert it, just stick it on the end.
                        if(not found):
                            self.degs.append(dgs[j][0] - dgs[j][1])
                            self.mats.append(zeros(2, 2))
                            self.mats[-1][i // 2, i % 2] = cfs[j]
                             
        # Technically, everything should already be ordered, but it's best to play it safe.
        if(len(self.degs) > 0):
            self.uLow = min(deg for deg in self.degs)
         
     
    # Returns the whole matrix found by recombining the series.
    def getWhole(self):
        matWhole = Matrix([[0, 0], [0, 0]])
        for i in range(0, len(self.degs), 1):
            matWhole += (u ** self.degs[i]) * self.mats[i]
         
        return matWhole
                             
         
    # Gets the coeeficient of this matrix of order n.
    def getCoeff(self, n):
        for i in range(0, len(self.degs), 1):
            if(self.degs[i] == n):
                return self.mats[i]
                 
        return zeros(2, 2)
         
         
    # Reverses all of the powers of u.
    def invLim(self):
        for i in range(0, len(self.degs), 1):
            self.degs[i] *= -1

        if(len(self.degs) > 0):
            self.uLow = min(deg for deg in self.degs)

        
    # Scales u by a given coefficient.
    def cffLim(self, cff):
        for i in range(0, len(self.degs), 1):
            self.mats[i] *= (cff ** self.degs[i])

