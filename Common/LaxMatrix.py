from sympy import Matrix, symbols
import Common.MatrixSeries as MatrixSeries

u = symbols("u")

# A class for holding and managing the Lax matrix.
class LaxMatrix:
    def __init__(self):
        self.diag = MatrixSeries.MatrixSeries()
        self.adiag = MatrixSeries.MatrixSeries()
        
    
    # Gets the lowest order of u.
    def uLow(self):
        return (self.diag.uLow if (self.diag.uLow < self.adiag.uLow) else self.adiag.uLow)
        
        
    # Given a whole matrix inpMat, split it into its diagonal and antidiagonal parts.
    def setMat(self, inpMat):
        self.diag.setMat(Matrix([[inpMat[0, 0], 0], [0, inpMat[1, 1]]]))
        self.adiag.setMat(Matrix([[0, inpMat[0, 1]], [inpMat[1, 0], 0]]))
        
    
    # Returns the whole matrix found by recombining the diagonal and anti-diagonal parts.
    def getWhole(self):
        return self.diag.getWhole() + self.adiag.getWhole()
                            
        
    # Gets the coeeficient of the (anti)diagonal part of the U-matrix.
    def getCoeff(self, diag, n):
        if(diag):
            return self.diag.getCoeff(n)
        else:
            return self.adiag.getCoeff(n)
        
        
    # Reverses all of the powers of u (multiplicatively).
    def invLim(self):
        self.diag.invLim()
        self.adiag.invLim()

        
    # Scales u by a given coefficient.
    def cffLim(self, cff):
        self.diag.cffLim(cff)
        self.adiag.cffLim(cff)

