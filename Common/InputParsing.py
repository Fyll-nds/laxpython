from sympy import Matrix, zeros, pprint
from sympy.parsing.sympy_parser import parse_expr
from sympy.parsing.sympy_tokenize import TokenError

# Read a list of inputs from the given file, and store them for later use.
fakeInpList = []
def readFakeInps(fileName):
    global fakeInpList
    try:
        fakeInpList = open(fileName, "r").read().split("\n")
        return True
    except FileNotFoundError:
        return False

# A wrapper for the regular Python "input()" function, in case I want to intervene.
def getInp():
    # If there are fake inputs, take the next one of these instead of reading a proper input.
    if(len(fakeInpList) > 0):
        hold = fakeInpList[0]
        fakeInpList.pop(0)
        return hold
    else:
        return input()


# Reads in an integer (without crashing).
def readInt():
    hold = getInp()

    # Keep looping until it doesn't throw a ValueError.
    while(True):
        try:
            return int(hold)
        # If they put in something illegal, try again.
        except ValueError:
            print("That's not a number!")
            hold = getInp()


# Read an input that can be parsed by parse_expr(), then parse it.
def parseInp():
    hold = getInp()

    # Keep looping until the inputted input is parseable.
    while(True):
        try:
            # If the input is a matrix, check for any prefactors.
            mulIndex = hold.find("[")
            if(mulIndex >= 0):
                mulIndex = hold[:mulIndex].rfind("*")

            # If there is a prefactor, multiply each element in the array by it.
            if(mulIndex >= 0):
                holdOut = parse_expr(hold[mulIndex + 1:])

                prefactor = parse_expr(hold[:mulIndex])
                for i in range(0, len(holdOut), 1):
                    for j in range(0, len(holdOut[i]), 1):
                        holdOut[i][j] *= prefactor
            # If there are no prefactors, or it's not a matrix, just parse it and be done.
            else:
                holdOut = parse_expr(hold)

            return holdOut
        except (TokenError, SyntaxError, ValueError):
            print("It seems there was a typo in your input (i.e. the parser failed to understand it). Please try again.")
            hold = getInp()


# Make sure to get an input of the requested type. Copied over from
def inpInList(inpList):
    inp = getInp().lower()
    while(inp not in inpList):
        print("Please pick one of the available options.")
        inp = getInp().lower()
        
    return inp


# A simple struct for holding a matrix (so that the InputParsing function can work with bare matrices).
class MatrixWrapper:
    def __init__(self):
        self.mat = zeros(2, 2)
    def setMat(self, mat):
        self.mat = mat
    def getWhole(self):
        return self.mat


# Handles the inputting of a 2x2 matrix.
def parseMatrix(mat, name):
    allClear = False
    print("")

    while(not allClear):
        matHold = Matrix([[0, 0], [0, 0]])
        for i in range(0, 4, 1):
            print("Please input the (" + str((i // 2) + 1) + ", " + str((i % 2) + 1) + ") element of the " + name + "-matrix" + (" (or input the whole matrix as a list '[[a, b], [c, d]]')" if i == 0 else "") + ":")
            holdInp = parseInp()

            # If a whole list was inputted, just parse that and be done.
            if(isinstance(holdInp, list)):
                matHold = Matrix(holdInp)
                break
            else:
                matHold[i // 2, i % 2] = holdInp
            
        mat.setMat(matHold)
        print("\nThis is the " + name + "-matrix you inputted, as the computer managed to read it. Is this correct ('y' or 'n')?")
        pprint(mat.getWhole())
        
        if(inpInList(["y", "n"]) == "y"):
            allClear = True
        else:
            print("")


# Handles the inputting of a 4x4 matrix.
def parse4Matrix(name):
    print("\nPlease input the " + name + "-matrix (e.g. the Yangian would be '1/u * [[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]]').")
    return Matrix(parseInp())

