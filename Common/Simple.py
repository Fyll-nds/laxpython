import sympy as sym
from sympy.parsing.sympy_parser import parse_expr

conds = []

# Attempt to simplify the given expression as much as possible by using any extra conditions added in by the user.
def applyConds(expr):
    # Always need to check for 0....
    if(expr == 0 or expr == None):
        return 0

    # Apply any conditions.
    for i in range(0, len(conds), 1):
        expr = expr.subs(conds[i][0], conds[i][1])

    # Convert any decimals to fractions, then expand and return the result (assuming it's non-constant, or else there're errors).
    # I don't like this, but I can't seem to find any better way to get around the errors....
    try:
        return sym.expand(sym.nsimplify(expr))
    except (AttributeError, RecursionError, ValueError):
        return expr


# Adds a condition to the list.
def addCond(cond):
    global conds

    # Split the condition into its lhs and rhs.
    equals = cond.find('=')
    conds.append([parse_expr(cond[:equals]), parse_expr(cond[(equals + 1):])])


# Checks if the given term contains any dependence on the variable x.
def hasxFuncDep(term, x):
    for arg in term.args:
        if(arg.func == sym.Symbol and arg == x):
            return True
        elif(hasxFuncDep(arg, x)):
            return True

    return False


# Splits the given term depending on whether it should be inside or outside the integral with respect to x.
def splitTermFromInt(term, x):
    # If it's a sum of terms, check each individually.
    if(term.func == sym.Add):
        oup = [0, 0, 0]
        for arg in term.args:
            hold = splitTermFromInt(arg, x)
            oup = [oup[0] + hold[0], oup[1] + hold[1], oup[2] + hold[2]]

        return oup
    # If it's just a derivative, it goes outside the integral.
    elif(term.func == sym.Derivative):
        return [0, 0, term]
    # If it's a product, check if it's a (scalar * diff).
    elif(term.func == sym.Mul):
        foundDiff = 0
        foundFunc = False
        for arg in term.args:
            if(arg.func == sym.Derivative):
                foundDiff += 1
            elif(hasxFuncDep(arg, x)):
                foundFunc = True
                break

        if(foundDiff == 1 and not foundFunc):
            return [0, 0, term]
        else:
            return [term, 0, 0]
    # If it is just a function, or has function dependence, then leave it inside the integral.
    elif(term.func == sym.Function or hasxFuncDep(term, x)):
        return [term, 0, 0]
    # Else, it must be some sort of scalar.
    else:
        return [0, x * term, 0]


# Attempts to remove any total derivative terms from the passed in expression (as if it were under an integral).
def fakeIntegral(expr, killDiffs = True):
    # Go through each term (as found by expanding any brackets), seperating everything that isn't a total derivative (into tdInt) or scalar (into scInt).
    x = sym.Symbol("x")
    [inInt, scInt, tdInt] = splitTermFromInt(sym.expand(expr), x)

    oup = 0
    L = sym.Symbol("L")

    # Finally, substitute the whole thing under an integral (but only if it's non-zero).
    if(inInt != 0):
        dummyVar = sym.Function("{{You shouldn't see this!}}")(x)
        oup = sym.integrate(dummyVar, (x, -L, L)).subs(dummyVar, inInt)

    if(scInt != 0):
        oup += scInt.subs(x, L) - scInt.subs(x, -L)

    if(not killDiffs and tdInt != 0):
        oup += tdInt.subs(x, L) - tdInt.subs(x, -L)

    return oup

