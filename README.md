# About

This is a program for calculating things in the Lax formalism.

The code was cobbled together by Iain Findlay (`iaf1@hw.ac.uk`) in Python using the [SymPy](www.sympy.org/) library. As a probably quite important note, I'm not usually a Python programmer, so my code likely looks horrible. It's all one big learning experience though, right? Also, I'm hopefully in the process of cleaning it up (honest!)....

To run the program, you will need to download the file `laxpython.zip`, and either `LaxPython_Windows.bat` or `LaxPython_Ubuntu.sh` depending on your operating system, from [this link](https://bitbucket.org/Fyll-nds/laxpython/downloads/). With these files in the same directory, you should only need to double click on the non-zip file, and it will run the rest of the code. Alternatively, you can run these in the command line by typing `python laxpython.zip` in the appropriate directory.

It will try to run the program with a graphical interface (written using TKinter) when run in this way, but if you want to use the command line interface (for whatever reason), you should only need to run it with the flag `--cli`, or just not have the TKinter libraries available (as it defaults to the command line if TKinter isn't available).

# What can this do at the moment?
* Calculate Hamiltonians and V-matrices for classical continuous systems with both open and closed boundary conditions.
* Check if a K-matrix satisfies the appropriate algebra (given the corresponding r-matrix).
* Check if a given r-/R-matrix satsifies the (classical) Yang-Baxter equation.
* Calculate the Poisson brackets for a system, given the Lax matrix and the r-matrix.
* Insert a Lax pair into the ZCC to find the equations of motion.

# Execution Notes:
* Because lambda is a bit long for a common parameter, the program interprets `u` as the spectral parameter (`u_2`, `x`, `t`, and `n` are also reserved).
* Relatedly, if you want a multiplicative spectral parameter (such as `exp(lambda)`), then just use `u = exp(lambda)` instead.
* When inputting the U-matrix, you will need to write `f(x)` rather than just `f` for the fields, as the computer needs to recognise that they're functions, and as such, can be differentiated. On this note, if you are considering any sort of discrete system, you should pass the index in as if it were a parameter (e.g. `f(n, t)`).
* As this is Python, to input powers you need to type `a ** b` to get `a` to the power of `b`. To input exponentials, type `exp(...)`, as doing `e ** (...)` will be read as defining some new variable `e`. Similarly, differentials are `diff(f(x), x)`.
* In the command line interface, if presented with multiple options when the choice isn't a specific "pick one" situation (e.g. 'V' and 'H'), you can enter 'V' to just read V, 'H', to just find H, 'VH' or 'HV' to get both, or neither to get neither.
* If you choose to find V-matrices, and then choose to input your own r-matrix (in the command line. This is the only option for now in the graphical version), the program will likely be quite slow, as it has to calculate the series expansion of the r-matrix at run time, rather than using one of the pre-done expansions I've hardcoded in.
* In the command line version, there will be an option at the end to preview the TeX'ed up results in a separate window (so don't worry about the hard to read ASCII art). While this is usually a lot nicer, sometimes the expressions can be too wide (or too numerous), and get cut off one way or another, which is a shame, but I can't seem to find any nice way around this (NOTE: this only seems to happen if you don't have a suitable .pdf reader installed).

# DISCLAIMER:
It is heavily recommended that you check any results that this returns by hand, as I can't guarantee that cases I haven't tested will work.

# TODO:
* Figure out how to do discrete systems better.
* Add in BTs.
* Account for the cases I haven't done so for yet (i.e. get rid of as many "I haven't bothered to do this yet" messages as possible).
* Clean up the interface (and the GUI)!
* Write a wiki?
