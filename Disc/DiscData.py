import sympy as sym
import Common.Simple as simp

u, n, m1, m2, L = sym.symbols("u n m1 m2 L")

# Try to use SymPy subs on this. Meant to catch the errors for when you are substituting in to a regular Python expression (like 0).
def trySubs(expr, subArr, useRep = True):
    try:
        if(not useRep):
            return expr.subs(*subArr)
        elif(len(subArr) == 2):
            return expr.replace(*subArr)
        else:
            hold = expr
            for subVals in subArr[0]:
                hold = hold.replace(*subVals)
            return hold
    except AttributeError:
        return expr


# A function for solving linear recursion relations containing arbitrary functions. SymPy's inbuilt rsolve() function can't handle these cases, so I have to do it manually. "expr" is the expression, "func" is the function that you want to recursively solve for, and iData is the initial data.
def myRSolve(expr, func, iData):
    # A quick catch for if it's 0.
    if(expr == 0):
        return 0

    # First, convert it to a polynomial, so that you can look at the coeffs.
    polyExpr = sym.Poly(expr, func.subs(n, n + 1), func)
    cfs = [ polyExpr.coeff_monomial(func.subs(n, n + 1)), -1 * polyExpr.coeff_monomial(func), -1 * polyExpr.coeff_monomial(1) ]

    if((cfs[0] == 0 and cfs[2] == 0) or (cfs[1] == 0 and cfs[2] == 0)):
        return 0
    elif(cfs[0] == 0):
        return -cfs[2] / cfs[1]
    elif(cfs[1] == 0):
        return (cfs[2] / cfs[0]).subs(n, n - 1)
    elif(cfs[2] == 0):
        return sym.product(trySubs(cfs[1] / cfs[0], (n, m1)), (m1, 1, n - 1)) * iData
    else:
        return (sym.product(trySubs(cfs[1] / cfs[0], (n, m1)), (m1, 1, n - 1)) * iData) + sym.Sum(sym.product(trySubs(cfs[1] / cfs[0], (n, m2)), (m2, m1 + 1, n - 1)) * trySubs(cfs[2] / cfs[0], (n, m1)), (m1, 1, n - 1))

 
# A class for holding the W and Z matrices used in the construction of the discrete Lax pair and Hamiltonians.
class DiscData:
    def __init__(self, LMat):
        self.reset()
        self.L = LMat

        # Keep track of the dummy variables, so that you can make sure that they're distinct.
        self.ns = [sym.symbols("n_0"), sym.symbols("n_1")]


    # Empties all of the lists that got built up.
    def reset(self):
        self.W = []
        self.IpWInvTerms = []

        self.Z = []


    # Check if the given L-matrix is suitable, and calculate the first order of everything (as the W expression is quadratic(ish) so is easiest done manually).
    def okayLMat(self):
        # For lowest order matrix [[a, b], [c, d]], the recurrence relations are:
        #   w12(n + 1) = (b + a*w12(n))/(d + c*w12(n)),
        #   w21(n + 1) = (c + d*w21(n))/(a + b*w21(n)).
        lowMat = self.L.getCoeff(True, self.L.uLow()) + self.L.getCoeff(False, self.L.uLow())

        # Diagonal -> b = c = 0.
        if(lowMat[0, 1] == 0 and lowMat[1, 0] == 0):
            if(lowMat[0, 0] == 0 or lowMat[1, 1] == 0):
                self.W.append(sym.zeros(2, 2))
            else:
                self.W.append(sym.Matrix([[0, sym.product(trySubs(lowMat[0, 0] / lowMat[1, 1], (n, self.ns[0])), (self.ns[0], 1, n - 1))],
                                          [sym.product(trySubs(lowMat[1, 1] / lowMat[0, 0], (n, self.ns[0])), (self.ns[0], 1, n - 1)), 0]]))
        # At least one of the entries is 0.
        elif(lowMat[0, 0] == 0 or lowMat[0, 1] == 0 or lowMat[1, 0] == 0 or lowMat[1, 1] == 0):
            return False
        # Decomposable into the outer product of two vectors -> d = bc/a.
        elif(simp.applyConds(lowMat.det()) == 0):
            if(lowMat[0, 0] == 0 or lowMat[1, 0] == 0):
                self.W.append(sym.zeros(2, 2))
            else:
                self.W.append(sym.Matrix([[0, lowMat[0, 0] / lowMat[1, 0]], [lowMat[1, 0] / lowMat[0, 0], 0]]))
        # If none of the above, then it's impossible.
        else:
            return False

        # Now that we know the matrix is okay, find the first Z as well (seeing as you have lowMat defined).
        self.Z.append(sym.zeros(2, 2))
        if(lowMat[0, 0] + (lowMat[0, 1] * self.W[0][1, 0]) != 0):
            self.Z[-1][0, 0] = sym.product(trySubs(lowMat[0, 0] + (lowMat[0, 1] * self.W[0][1, 0]), (n, self.ns[1])), (self.ns[1], 1, n - 1))
        if(lowMat[1, 1] + (lowMat[1, 0] * self.W[0][0, 1]) != 0):
            self.Z[-1][1, 1] = sym.product(trySubs(lowMat[1, 1] + (lowMat[1, 0] * self.W[0][0, 1]), (n, self.ns[1])), (self.ns[1], 1, n - 1))

        return True


    # Adds a term to the W series.
    def addW(self):
        order = len(self.W)

        for i in range(0, 2, 1):
            self.ns.append(sym.symbols("n_" + str(len(self.ns))))

        # Add a dummy matrix to the list.
        w12 = sym.Function("w12")(n)
        w21 = sym.Function("w21")(n)
        self.W.append(sym.Matrix([[0, w12], [w21, 0]]))

        # Set up the difference equation: A_n + D_n W_n - W_{n + 1} D_n - W_{n + 1} A_n W_n = 0.
        holdEq = self.L.getCoeff(False, order)
        for i in range(0, order + 1, 1):
            holdEq += self.L.getCoeff(True, order - i) * self.W[i]

        for i in range(0, order + 1, 1):
            holdEq -= self.W[i].subs(n, n + 1) * self.L.getCoeff(True, order - i)

        for i in range(0, order + 1, 1):
            for j in range(0, order - i + 1, 1):
                holdEq -= self.W[i].subs(n, n + 1) * self.L.getCoeff(False, order - i - j) * self.W[j]

        # Now solve the recurrence relations to find the actual values for w12 and w21.
        self.W[-1][0, 1] = simp.applyConds(trySubs(myRSolve(holdEq[0, 1], w12, 0), [[(m1, self.ns[-1]), (m2, self.ns[-2])]]))
        self.W[-1][1, 0] = simp.applyConds(trySubs(myRSolve(holdEq[1, 0], w21, 0), [[(m1, self.ns[-1]), (m2, self.ns[-2])]]))
 
 
    # Add a term to the Z series.
    def addZ(self):
        order = len(self.Z)

        for i in range(0, 2, 1):
            self.ns.append(sym.symbols("n_" + str(len(self.ns))))

        # Add a dummy matrix to the list.
        z11 = sym.Function("z11")(n)
        z22 = sym.Function("z22")(n)
        self.Z.append(sym.Matrix([[z11, 0], [0, z22]]))

        # Set up the difference equation: Z_{n + 1} - D_n Z_n - A_n W_n Z_n = 0.
        holdEq = self.Z[order].subs(n, n + 1)
        for i in range(0, order + 1, 1):
            holdEq -= self.L.getCoeff(True, order - i) * self.Z[i]

        for i in range(0, order + 1, 1):
            for j in range(0, order + 1, 1):
                holdEq -= self.L.getCoeff(False, order - i - j) * self.getW(i) * self.Z[j]

        # Now solve the recurrence relations to find the actual values for z11 and z22.
        self.Z[-1][0, 0] = simp.applyConds(trySubs(myRSolve(holdEq[0, 0], z11, 0), [[(m1, self.ns[-1]), (m2, self.ns[-2])]]))
        self.Z[-1][1, 1] = simp.applyConds(trySubs(myRSolve(holdEq[1, 1], z22, 0), [[(m1, self.ns[-1]), (m2, self.ns[-2])]]))
             
 
    # Gets the W-matrix of the given index, and adds one if there isn't already one.
    def getW(self, n):
        if(n < 0):
            return sym.zeros(2, 2)
 
        while(n >= len(self.W)):
            self.addW()
 
        return self.W[n]
             
 
    # Gets the Z-matrix of the given index, and adds one if there isn't already one.
    def getZ(self, n):
        if(n < 0):
            return sym.zeros(2, 2)
 
        while(n >= len(self.Z)):
            self.addZ()
 
        return self.Z[n]
         
     
    # For getting (I + W).
    def getIpW(self, n):
        if(n == 0):
            return sym.eye(2) + self.getW(0)
        else:
            return self.getW(n)
             
 
    # Recursion function used in getIpWInv().
    def recFuncIpW(self, n):
        out = -1 * self.getW(n)
        for i in range(1, n, 1):
            out -= self.getW(i) * self.recFuncIpW(n - i)
 
        return out
 
 
    # Gets the appropriate term from (I + W)^(-1) of order n.
    def getIpWInv(self, n):
        if(n < 0):
            return 0
        elif(len(self.IpWInvTerms) == 0):
            self.IpWInvTerms.append((sym.eye(2) + self.getW(0)).inv())

        while(n >= len(self.IpWInvTerms)):
            self.IpWInvTerms.append(self.IpWInvTerms[0] * self.recFuncIpW(len(self.IpWInvTerms)) * self.IpWInvTerms[0])

        return self.IpWInvTerms[n]

