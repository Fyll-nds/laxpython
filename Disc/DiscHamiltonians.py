import sympy as sym
import Common.Simple as simp

# A class for managing all of the Hamiltonians.
class Hamiltonians:
    def __init__(self, data):
        self.hams = []
        self.data = data


    # Recursion function used to find the term of order n from the logarithm.
    def recFuncLog(self, n, depth = 1):
        out = self.getHam(n, False) / (depth * self.getHam(0, False))
        for i in range(1, n, 1):
            out -= self.getHam(i, False) * self.recFuncLog(n - i, depth + 1) / self.getHam(0, False)

        return out


    # Finds and returns (recursively) a list of all of the summation indices present in the given expression.
    def cleanHamIndicesRec(self, expr):
        indices = set([])
        # If this is a sum, make a note of the summation variable. Then, continue.
        if(expr.func == sym.Sum):
            indices = self.cleanHamIndicesRec(expr.args[0])
            indices |= set([expr.args[1][0]])
        else:
            for arg in expr.args:
                indices |= self.cleanHamIndicesRec(arg)

        return indices


    # Add a new Hamiltonian to the list.
    def addHam(self):
        n = len(self.hams)
        self.hams.append(simp.applyConds(sym.trace(self.data.getZ(n).subs(sym.Symbol("n"), sym.Symbol("L") + 1))))

        # The data contains a lot of redundant indices. Clean them out here (if it actually has any).
        if(isinstance(self.hams[-1], sym.Basic)):
            indices = list(self.cleanHamIndicesRec(self.hams[-1]))
            # Sort the indices in increasing numerical order (to avoid any clashes during the relabelling).
            indices.sort(key = (lambda x : int(x.name[2:])))

            for i in range(0, len(indices), 1):
                self.hams[-1] = self.hams[-1].replace(indices[i], self.data.ns[i])


    # Get the nth Hamiltonian.
    def getHam(self, n, useLog):
        if(n < 0):
            return 0

        # Make sure you have enough Hamiltonians.
        while(n >= len(self.hams)):
            self.addHam()

        if(useLog):
            # 0 is a special case.
            if(n == 0):
                return sym.log(self.hams[0])
            else:
                return self.recFuncLog(n)
        # Lovely easy non-log.
        else:
            return self.hams[n]

