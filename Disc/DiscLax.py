import sympy as sym

import Common.LaxMatrix as LaxMatrix
import Disc.DiscHamiltonians as DiscHams
import Disc.DiscData as DiscData
import Common.Simple as simp
import Common.InputParsing as inp
import Common.OutputStyling as oup

# The spectral parameter(s if using r-matrices) and variables.
u, u_2, x, L = sym.symbols("u u_2 x L")

# Declared up here, but actually initialised down once things has been passed in.
LMat = LaxMatrix.LaxMatrix()
invLim = False

# A storage container for all of the W and Z matrices.
discData = DiscData.DiscData(LMat)

# The Hamiltonians.
logHams = False
hams = DiscHams.Hamiltonians(discData)


#---------------------------------------------------------------------------------------------------------------------#
#                                          --v-- The function calling --v--                                           #
#---------------------------------------------------------------------------------------------------------------------#

# Converts a number into a bracketed index (and reverses it if you're in the opposite limit).
def nToIndex(n):
    if(invLim):
        n *= -1
    if(n < 0 or n > 9):
        return "{" + str(n) + "}"
    else:
        return str(n)


# Functions for passing in to the below function.
def getHam(i):
    global logHams, hams
    return hams.getHam(i, logHams)


# Prints the information to be printed.
def innerPrintFunc(func, low, high, printChar, cliOut, texOut, texName):
    if(cliOut):
        oup.printSepLine()
    if(texOut):
        oup.addTexText(" \\textbf{" + texName + ":} \\\\ ")

    for i in range(low, high + 1, 1):
        holdText = printChar + "_" + nToIndex(i)
        holdOut = func(i)

        # If you're printing to the terminal, do so.
        if(cliOut):
            print("\n" + holdText + ":")
            sym.pprint(holdOut)

        # If you're printing to TeX, do so.
        if(texOut):
            oup.addTexEq(holdText, holdOut)


# Prints out everything requested with spacings.
def printDiscResults(n, which, cliOut, texOut):
    # Now try fetching the number of Ham.'s requested, if asked.
    if(which[0]):
        innerPrintFunc(getHam, 0, n, 'H', cliOut, texOut != "n", "Hamiltonians")

    # Preview the TeX if you've been asked.
    if(texOut != "n"):
        oup.previewTex(texOut[1:])


#---------------------------------------------------------------------------------------------------------------------#
#                                             --v-- The input reading --v--                                           #
#---------------------------------------------------------------------------------------------------------------------#

# Actually initialise all of the variables.
def initDisc(inpMats, inpNonMats):
    # Allow this function to edit the globals.
    global LMat, discData, invLim, logHams

    # Empty all of the previously calculated results (in case you're running through the GUI).
    discData.reset()

    # Set up the L-matrix.
    LMat.setMat(inpMats[0].getWhole())

    # If they actually want the u -> inf limit, just invert all of the u's.
    if(inpNonMats[3].find("inf") >= 0):
        invLim = True
        LMat.invLim()

    # Rescale the L-matrix so that the lowest order of u is u^0.
    oldLow = LMat.uLow()
    LMat.diag.uLow -= oldLow
    LMat.adiag.uLow -= oldLow
    for i in range(0, len(LMat.diag.degs), 1):
        LMat.diag.degs[i] -= oldLow
    for i in range(0, len(LMat.adiag.degs), 1):
        LMat.adiag.degs[i] -= oldLow

    # Make sure that the choice of L-matrix is suitable (i.e., that the computer can systematically find things from it).
    if(not discData.okayLMat()):
        return False

    # Whether or not you want the logarithmic generator.
    logHams = inpNonMats[5]

    return True


# Actually gather the inputs.
def runDisc():
    # Listed objects: [LMat, rMat, KMats[0], KMats[1]], [openBCs, additive parameter, rMat type, invLim, limit factor, logGen]
    # Most of these are useless at the moment, but are left in for compatibility.
    inpMats = [inp.MatrixWrapper(), sym.zeros(4, 4), inp.MatrixWrapper(), inp.MatrixWrapper()]
    inpNonMats = [False, True, " ", "0", 1.0, False]

    # Get the entries of the L-matrix.
    inp.parseMatrix(inpMats[0], "L(n)")
    
    # Get any conditions on the fields.
    print("\nAnd are there any extra conditions on the fields ('y'/'n')?")
    if(inp.inpInList(["y", "n"]) == "y"):
        print("\nThen please enter them, one at a time (e.g. 'a(x) + b(x) = 1'), until you are done (and then just enter a blank).")
        holdInp = inp.getInp()

        while(holdInp != ""):
            simp.addCond(holdInp)
            holdInp = inp.getInp()

    print("\nIs the spectral parameter additive ('+') or multiplicative ('*')?")
    inpNonMats[1] = (inp.inpInList(["+", "*"]) == "+")

    # Which limit would you like to consider, and are the Ham.s generated by the log?
    print("\nWould you like to consider the limit as u -> 0 or u -> infinity? Please enter either '0' or 'inf'.")
    inpNonMats[3] = inp.inpInList(["0", "inf"])

    # Are the Hamiltonians generated by the log or not?
    print("\nAnd are the Hamiltonians generated by the logarithm of the transfer matrix ('y'/'n')?")
    inpNonMats[5] = (inp.inpInList(["y", "n"]) == 'y')

    # Do you want the Hamiltonians, and/or the A(n)-matrices?
    #print("\nDo you want to be given Hamiltonians ('H'), and/or A(n)-matrices ('A')?")
    #optsHA = inp.getInp()
    optsHA = "H"

    # If so, fetch the r-matrix.
    if('A' in optsHA):
        print("I haven't started working on the A(n)-matrices yet.")

    # Find out how many terms to find.
    print("\nWhat is the highest order of " + ("1/u" if (inpNonMats[3] == "inf") else "u") + " you wish to calculate up to?")
    nTerms = inp.readInt()

    # Do you want TeX?
    print("\nWould you like to try to view the results with LaTeX formatting ('y'/'n')?")
    doTex = inp.inpInList(["y", "n"])
    if(doTex == "save"):
        print("\nPlease enter the file name to use.")
        doTex = "s" + inp.getInp()

    # Initialise everything. If this fails, the L-matrix was not applicable.
    if(not initDisc(inpMats, inpNonMats)):
        print("The computer doesn't know how to work with that choice of L-matrix and limit. Sorry.")
        return

    # Run the actual program.
    printDiscResults(nTerms, ['H' in optsHA, 'A' in optsHA], True, doTex)

