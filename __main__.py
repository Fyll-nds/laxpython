from Common.InputParsing import readFakeInps
from Common.OutputStyling import boxedOup
import argparse

parser = argparse.ArgumentParser(description = "A program for performing calculations in the Lax Formalism.", epilog = "Visit \"https://bitbucket.org/Fyll-nds/laxpython/overview\" for full details and usage notes.")
parser.add_argument("--cli", action = "store_true", help = "Force the program to run using the command line interface rather than the GUI. If TKinter is not installed, it will fall through to this option anyway.")
parser.add_argument("--file", dest = "filename", help = "Reads the given file and uses its lines as the inputs to the program. This option forces the program to use --cli.")
args = parser.parse_args()

useCLI = args.cli
fileName = args.filename

# If they've piped an input in, load the contents into a fake input list and force it to use the CLI.
if(fileName):
    if(readFakeInps(fileName)):
        useCLI = True
    else:
        boxedOup("Unable to load the file '" + fileName + "'.")
        quit()

# Check if you want (and can access) the GUI.
if(not useCLI):
    try:
        import GUI.GUI
        win = GUI.GUI.Window("LaxPython")
        win.run()
    except ImportError:
        boxedOup("Failed to load the TKinter library. Using the command line interface...")
        useCLI = True

# If you failed (or it was requested), run the CLI.
if(useCLI):
    from Cts.CtsLax import runCts
    from Disc.DiscLax import runDisc
    from Alg.AlgLax import runAlg
    from BTs.BTLax import runBTs
    from Alg.ZCC import runEoMs
    from Common.InputParsing import inpInList
    from Common.OutputStyling import printSepLine

    # Mark off where it starts for ease of reading. Also write out a general overview message.
    print(""" _____________________________________________________________________________ 
/                                                                             \\
|   This is a program to calculate and check things in the Lax formalism. The |
| code was cobbled together by Iain Findlay (iaf1@hw.ac.uk) in Python using   |
| the SymPy (www.sympy.org) library.                                          |
|   For usage notes, see: https://bitbucket.org/Fyll-nds/laxpython/overview   |
|                                                                             |
| DISCLAIMER: It is heavily recommended that you check any results that this  |
| returns by hand, as I can't guarantee that cases I haven't tested work.     |
\\_____________________________________________________________________________/""")

    # What would you like to do?
    print("\nWhat would you like to do? Your choices are:")
    print("  - Calculate continuous Hamiltonians/V-matrices. ('c')")
    print("  - Calculate discrete Hamiltonians/V-matrices. ('d') <- Only Hamiltonians.")
    print("  - Find Poisson brackets/check algebraic relations. ('a')")
    #print("  - Perform a Backlund transformation. ('b') <- Not Implemented.")
    print("  - Find the equations of motion for a system (from the Lax pair). ('e')")
    choice = inpInList(["c", "d", "a", "b", "e"])

    # Fall through to the appropriate function.
    if(choice == 'c'):
        runCts()
    elif(choice == 'd'):
        runDisc()
    elif(choice == 'a'):
        runAlg()
    elif(choice == 'b'):
        runBTs()
    elif(choice == 'e'):
        runEoMs()

    # Mark off where it ends for ease of reading.
    printSepLine()

    # Hold for exit.
    print("Press Enter to exit.")
    input()

